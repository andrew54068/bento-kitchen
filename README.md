# Bento Kitchen
This is a [Next.js](https://nextjs.org) project bootstrapped with [`create-wagmi`](https://github.com/wevm/wagmi/tree/main/packages/create-wagmi).

## Params Information

- Referral address: 0x80011844928B469EAc5E4bC7e6EBA9b3C2Fa1b41
- Zircuit invite code: BENTOZ
