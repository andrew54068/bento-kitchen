import { createTheme } from "@mantine/core";

export const light = createTheme({
  focusRing: "auto",
  defaultRadius: "md",

  fontFamily: "Roboto, sans-serif",
  headings: {
    fontFamily: "Roboto, sans-serif",
  },
});

export const geek = createTheme({
  focusRing: "auto",
  scale: 0.9,
  // defaultRadius: "md",

  fontFamily: "Share Tech Mono, monospace",
  headings: {
    fontFamily: "Share Tech Mono, monospace",
  },
});
