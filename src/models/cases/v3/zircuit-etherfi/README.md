Zircuit x EtherFi

## Sample Transaction

- https://etherscan.io/tx/0xdd4e435235416fa0ea1f026dea356785c2ab196c08122aaaee0f7dc23ff17e71

## References

- [Zircuit](https://stake.zircuit.com/)
- [EtherFi](https://app.ether.fi/eeth/)
