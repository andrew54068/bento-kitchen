import { Chain } from "wagmi/chains";
import { type UseAccountReturnType } from "wagmi";
import { parseUnits, formatUnits } from "viem";
import { BatchCase, Context } from "./types";

export const decimalValidator =
  (decimal: number, min?: bigint, max?: bigint) => (value: string) => {
    // validate
    // 1. is number
    // 2. is in range
    // 3. is with specified decimal places
    let num = parseUnits(value, decimal);
    if (min !== undefined && num < min) {
      throw new Error(`Number must be greater than ${formatUnits(min, decimal)}`);
    }
    if (max !== undefined && num > max) {
      throw new Error(`Number must be less than ${formatUnits(max, decimal)}`);
    }
    let [int, dec] = value.split(".");
    if (dec && dec.length > decimal) {
      throw new Error(`Number must have ${decimal} decimal places`);
    }
  };

// addressValidtor if value is a valid ethereum address
export const addressValidator = (value: string) => {
  // validate
  // 1. is ethereum address
  if (!value.startsWith("0x")) {
    throw new Error("Invalid ethereum address");
  }
  if (value.length !== 42) {
    throw new Error("Invalid ethereum address");
  }
  return;
};

export const integerValidator = (value: string) => {
  // check if string is a number
  if (isNaN(Number(value))) {
    throw new Error("Invalid number");
  }
  // check if number is integer
  if (value.includes(".")) {
    throw new Error("Only support integer");
  }
  return +value;
}

export const buildContext = (
  batchCase: BatchCase,
  account: UseAccountReturnType,
  inputs: string[],
  chains: readonly Chain[]
): Context => {
  let chain = chains.find((c) => c.id === batchCase.networkId);
  if (!chain) {
    throw new Error(`Chain not found`);
  }
  return {
    account,
    chain,
    inputs,
  };
};
