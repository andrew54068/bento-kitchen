import type { Abi, Address } from "abitype";
import { Chain, mainnet, polygon } from "wagmi/chains";
import {
  parseEther,
  parseUnits,
  getContract,
  createPublicClient,
  encodeFunctionData,
  http,
} from "viem";

import {
  Context,
  BatchCase,
  InputType,
  Tag,
  Tx,
} from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import YearnV3Vault from "./abi/YearnV3Vault.json";
import ERC20 from "@/models/abi/ERC20.json";
import yearnWithdrawal from "../yearn-withdrawal/index"

const buildYearnV3VaultCase = (
  id: string,
  name: string,
  description: string,
  blockchain: Chain,
  tokenName: string,
  tokenDecimals: number,
  tokenContract: Address,
  vaultName: string,
  vaultContract: Address
): BatchCase => {
  return {
    id: id,
    name: name,
    description: description,
    website: {
      title: "Yearn.Fi",
      url: "https://yearn.fi",
    },
    tags: ["DeFi", "Yield Farming", "Yearn V3", tokenName].map((str) => {
      return { title: str };
    }),
    networkId: blockchain.id,
    atomic: true,
    inputs: [
      {
        name: "Amount",
        inputType: InputType.ERC20Amount,
        description: "Amount to invest",
        validate: decimalValidator(tokenDecimals, BigInt(0)),
        options: {
          token: tokenContract,
        },
      },
    ],
    render: async (context: Context) => {
      // parse amount to BigInt
      // 1. parse as float
      // 2. multiply by 10^6
      // 3. convert to BigInt
      let amountBN = parseUnits(context.inputs[0], tokenDecimals);

      let txs: Tx[] = [];
      // approve tx
      txs.push({
        name: `Approve ${tokenName}`,
        description: `Approve Yearn V3 ${vaultName} vault to spend ${tokenName}`,
        to: tokenContract,
        value: BigInt(0),
        data: encodeFunctionData({
          abi: ERC20,
          functionName: "approve",
          args: [vaultContract, amountBN],
        }),
        abi: ERC20 as Abi,
      });

      // deposit tx
      txs.push({
        name: `Deposit ${tokenName}`,
        description: `Deposit ${tokenName} to Yearn V3 ${vaultName} vault`,
        to: vaultContract,
        value: parseEther("0"),
        data: encodeFunctionData({
          abi: YearnV3Vault,
          functionName: "deposit",
          args: [amountBN, context.account.address],
        }),
        abi: YearnV3Vault as Abi,
      });
      return txs;
    },
  };
};

const caseApproveAndDepositYearnV3USDCe: BatchCase = buildYearnV3VaultCase(
  "yearn_v3_usdce",
  "Yearn V3 USDC.e",
  "A batch to approve and deposit USDC.e to Yearn V3 USDC.e-A Vault on Polygon",
  polygon,
  "USDC.e",
  6,
  "0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174",
  "USDC.e-A",
  "0xA013Fbd4b711f9ded6fB09C1c0d358E2FbC2EAA0"
);

const caseApproveAndDepositYearnV3USDT: BatchCase = buildYearnV3VaultCase(
  "yearn_v3_usdt_a",
  "Yearn V3 USDT",
  "A batch to approve and deposit USDT to Yearn V3 USDT-A Vault on Polygon",
  polygon,
  "USDT",
  6,
  "0xc2132D05D31c914a87C6611C10748AEb04B58e8F",
  "USDT-A",
  "0xBb287E6017d3DEb0e2E65061e8684eab21060123"
);

const caseApproveAndDepositYearnV3DAI: BatchCase = buildYearnV3VaultCase(
  "yearn_v3_dai_a",
  "Yearn V3 DAI",
  "A batch to approve and deposit DAI to Yearn V3 DAI-A Vault on Polygon",
  polygon,
  "DAI",
  18,
  "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063",
  "DAI-A",
  "0x90b2f54C6aDDAD41b8f6c4fCCd555197BC0F773B"
);

const caseApproveAndDepositYearnV3WETH: BatchCase = buildYearnV3VaultCase(
  "yearn_v3_weth",
  "Yearn V3 WETH",
  "A batch to approve and deposit WETH to Yearn V3 WETH-A Vault on Polygon",
  polygon,
  "WETH",
  18,
  "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
  "WETH-A",
  "0x305F25377d0a39091e99B975558b1bdfC3975654"
);

/*
const caseApproveAndDepositYearnJuicedDAI = {
  id: "approve_and_deposit_yearn_ajna_dai_extra_apy",
  name: "Yearn Juiced Vault DAI Extra APY",
  description:
    "Approve and deposit DAI to Yearn Juiced Vault, then deposit yvAjnaDAI to earn extra APR",
  tags: [Tags.DeFi, Tags.YieldFarming, Tags.YearnJuice, Tags.AJNA, "DAI"],
  blockchain: mainnet,
  atomic: true,
  inputs: [
    {
      id: "amount",
      name: "Amount",
      inputType: InputType.ERC20Amount,
      description: "Amount to invest",
      placeholder: "100.001",
      validate: decimalValidator(18, BigInt(0)),
      options: {
        chain: mainnet,
        token: "0x6B175474E89094C44Da98b954EedeAC495271d0F" as Address,
      },
    },
  ],
  renderExpiry: 15,
  render: async (context: Context, inputs: Map<string, string>) => {
    let tokenContract: Address = "0x6B175474E89094C44Da98b954EedeAC495271d0F";
    let vaultContract: Address = "0xe24BA27551aBE96Ca401D39761cA2319Ea14e3CB";
    let extraAPYVaultContract: Address =
      "0x082a5743aAdf3d0Daf750EeF24652b36a68B1e9C";
    // let extraAJNAVaultContract: Address =
    //   "0x082a5743aAdf3d0Daf750EeF24652b36a68B1e9C";
    const client = createPublicClient({ chain: mainnet, transport: http() });
    // parse amount to BigInt
    // 1. parse as float
    // 2. multiply by 10^6
    // 3. convert to BigInt
    let amountBN = parseUnits(inputs.get("amount")!, 18);
    let slipageToleranceBase = 10000;
    let slipageTolerance = 1;

    let txs: Tx[] = [];
    // 1. Approve DAI
    // ex. https://etherscan.io/tx/0xc7714154011d6aa18aee77f339ad1a63975055cb7626fd97349bca3711ea61fb
    txs.push({
      name: "Approve DAI",
      description: "Approve Yearn Juiced Vault to spend DAI",
      to: "0x6B175474E89094C44Da98b954EedeAC495271d0F",
      value: parseEther("0"),
      call: {
        abi: {
          name: "approve",
          type: "function",
          inputs: [
            {
              name: "spender",
              type: "address",
            },
            {
              name: "amount",
              type: "uint256",
            },
          ],
          outputs: [
            {
              name: "",
              type: "bool",
            },
          ],
          stateMutability: "nonpayable",
        },
        functionArgs: [
          { name: "spender", value: vaultContract },
          { name: "amount", value: amountBN },
        ],
      },
    });

    // 2. Deposit DAI
    // ex. https://etherscan.io/tx/0xa10ebe325cb8b501a63414cd406bebcb38ce1263f6895d3558bd1d2319379ba7
    txs.push({
      name: "Deposit DAI to Yearn-Ajna DAI Vault",
      description: "Deposit DAI to Yearn-Ajna DAI Vault and mint yvAjnaDAI",
      to: vaultContract,
      value: parseEther("0"),
      call: {
        abi: {
          name: "deposit",
          type: "function",
          inputs: [
            {
              name: "assets",
              type: "uint256",
            },
            {
              name: "receiver",
              type: "address",
            },
          ],
          outputs: [
            {
              name: "",
              type: "uint256",
            },
          ],
          stateMutability: "nonpayable",
        },
        functionArgs: [
          { name: "assets", value: amountBN },
          { name: "receiver", value: context.account.address },
        ],
      },
    });

    const yearnV3Vault = getContract({
      address: vaultContract,
      abi: YearnV3Vault,
      client,
    });
    let estLPAmount = (await yearnV3Vault.read.previewDeposit([
      amountBN,
    ])) as bigint;
    estLPAmount =
      (estLPAmount * BigInt(slipageToleranceBase - slipageTolerance)) /
      BigInt(slipageToleranceBase);

    // 3. Approve Extra APR Vault to spend yvAjnaDAI
    // ex. https://etherscan.io/tx/0x6f81df0700a1b2f8a13dc527e80cc8b1ad0ea1361fa986de40103fce90f3fa68
    txs.push({
      name: "Approve yvAjnaDAI",
      description: "Approve Extra APR Vault to spend yvAjnaDAI",
      to: vaultContract,
      value: parseEther("0"),
      call: {
        abi: {
          name: "approve",
          type: "function",
          inputs: [
            {
              name: "spender",
              type: "address",
            },
            {
              name: "amount",
              type: "uint256",
            },
          ],
          outputs: [
            {
              name: "",
              type: "bool",
            },
          ],
          stateMutability: "nonpayable",
        },
        functionArgs: [
          { name: "spender", value: extraAPYVaultContract },
          { name: "amount", value: estLPAmount },
        ],
      },
    });

    // 4. Deposit yvAjnaDAI to Extra APR Vault
    //    function deposit(uint256 assets,address receiver)
    // ex. https://etherscan.io/tx/0x8a1244aa9a7b65d4ebc4cdd799bbdd825ce3054ff9a770ae1b6b967b839cdf19
    txs.push({
      name: "Deposit yvAjnaDAI to Extra APR Vault",
      description: "Deposit yvAjnaDAI to Extra APR Vault",
      to: extraAPYVaultContract,
      value: parseEther("0"),
      call: {
        abi: {
          name: "deposit",
          type: "function",
          inputs: [
            {
              name: "assets",
              type: "uint256",
            },
            {
              name: "receiver",
              type: "address",
            },
          ],
          outputs: [
            {
              name: "",
              type: "uint256",
            },
          ],
          stateMutability: "nonpayable",
        },
        functionArgs: [
          { name: "assets", value: estLPAmount },
          { name: "receiver", value: context.account.address },
        ],
      },
    });
    return txs;
  },
};*/

const yearnCases: BatchCase[] = [
  caseApproveAndDepositYearnV3USDCe,
  caseApproveAndDepositYearnV3USDT,
  caseApproveAndDepositYearnV3DAI,
  caseApproveAndDepositYearnV3WETH,
  ...yearnWithdrawal,
];

export default yearnCases;
