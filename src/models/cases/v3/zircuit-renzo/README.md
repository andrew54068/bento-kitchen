Zircuit x Renzo

## Sample Transaction

- https://etherscan.io/tx/0x85afb32050b24704fc0c0bcc1f9485d50ec36f4dafb09c0b624969eaf1fffb12

## References

- [Zircuit](https://stake.zircuit.com/)
- [Renzo](https://app.renzoprotocol.com/restake/)
