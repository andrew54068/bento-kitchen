import type { Abi, Address } from "abitype";
import {
  Context,
  BatchCase,
  InputType,
  Tag,
  Tx,
} from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import { ReferalAccount } from "@/models/cases/v3/constants";
import {
  encodeFunctionData,
  fromHex,
  parseUnits,
} from "viem";
import Lido from "./abi/Lido.json";
import EigenpieStaking from "./abi/EigenpieStaking.json";


const eigenpieSTETH: BatchCase = {
  id: "eigenpie_steth",
  name: "Eigenpie stETH restaking",
  description:
    "Deposit ETH to Earn Points from Eigenpie. <BR>\
    step 1. Deposit ETH into Lido smart contract to get $stETH <BR>\
    step 2. Approve $stETH to Eigenpie restaking <BR>\
    step 3. Deposit $stETH into Eigenpie",
  website: {
    title: "Eigenpie",
    url: "https://www.eigenlayer.magpiexyz.io/",
  },
  tags: ["Ethereum", "LRT", "DeFi", "ETH", "stETH"].map(
    (name) => ({ title: name } as Tag)
  ),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: 1,
  atomic: true,
  renderExpiry: 15,
  inputs: [
    {
      name: "ETH Amount",
      inputType: InputType.NativeAmount,
      description:
        "Amount to stake",
      validate: decimalValidator(18, BigInt(0)),
    },
  ],
  render: async (context: Context) => {
    let txs: Tx[] = [];
  
    const v = parseUnits(context.inputs[0], 18);
  
    // Lido
    const stETHAddr = "0xae7ab96520DE3A18E5e111B5EaAb095312D7fE84";
    // eigenpie
    const eigenpieAddr = "0x24db6717dB1C75B9Db6eA47164D8730B63875dB7";
    const estimateSTETHAmount = v * BigInt(992) / BigInt(1000);
    const estimateMSTETHAmount = estimateSTETHAmount * BigInt(990) / BigInt(1000);
  
    txs.push({
      name: "Stake to Lido",
      description: "Stake to Lido and wrap ETH to stETH",
      to: stETHAddr,
      value: v,
      data: encodeFunctionData({
        abi: Lido,
        functionName: 'submit',
        args:[ReferalAccount]
      }),
      abi: Lido as Abi,
    })
  
    txs.push({
      name: "Approve stETH",
      description: "approve stETH to Eigenpie",
      to: stETHAddr,
      value: 0n,
      data: encodeFunctionData({
        abi: Lido,
        functionName: 'approve',
        args:[
          eigenpieAddr,
          estimateSTETHAmount,
        ]
      }),
      abi: Lido as Abi,
    })
  
    txs.push({
      name: "Stake to Eigenpie",
      description: "Stake stETH to Eigenpie and wrap ETH to mstETH",
      to: eigenpieAddr,
      value: 0n,
      data: encodeFunctionData({
        abi: EigenpieStaking,
        functionName: 'depositAsset',
        args:[
          stETHAddr,
          estimateSTETHAmount,
          estimateMSTETHAmount,
          ReferalAccount
        ]
      }),
      abi: EigenpieStaking as Abi,
    })
    return txs;
  },
};

export default [eigenpieSTETH];
