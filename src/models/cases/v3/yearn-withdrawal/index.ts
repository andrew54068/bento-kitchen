import type { Abi, Address } from "abitype";
import { Context, BatchCase, InputType, Tag, Tx } from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import { createPublicClient, encodeFunctionData, formatUnits, getContract, http, parseUnits } from "viem";
import yvDAI from "./abi/yvDAI-A.json";
import yvUSDC from "./abi/yvUSDC-A.json";
import yvUSDT from "./abi/yvUSDT-A.json";
import yvWETH from "./abi/yvWETH-A.json";

const yearnWithdrawal: BatchCase = {
  id: "yearn_withdrawal",
  name: "Withdraw Yearn valuts",
  description: "Redeem your USDC.e, USDT, DAI and WETH in one transaction",
  website: {
    title: "Yearn",
    url: "https://yearn.fi/v3",
  },
  tags: ["DeFi", "USDC.e", "USDT", "DAI", "WETH"].map((name) => ({ title: name } as Tag)),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: 137,
  atomic: true,
  renderExpiry: undefined,
  inputs: [
    // {
    //   name: "yvDAI-A Amount",
    //   inputType: InputType.ERC20Amount,
    //   description: "Amount to withdraw",
    //   validate: decimalValidator(18, BigInt(0)),
    //   options: {
    //     token: "0x90b2f54C6aDDAD41b8f6c4fCCd555197BC0F773B",
    //   },
    // },
    // {
    //   name: "yvUSDC-A Amount",
    //   inputType: InputType.ERC20Amount,
    //   description: "Amount to withdraw",
    //   validate: decimalValidator(6, BigInt(0)),
    //   options: {
    //     token: "0xA013Fbd4b711f9ded6fB09C1c0d358E2FbC2EAA0",
    //   },
    // },
    {
      name: "yvUSDT-A Amount",
      inputType: InputType.ERC20Amount,
      description: "Amount to withdraw",
      validate: decimalValidator(6, BigInt(0)),
      options: {
        token: "0xBb287E6017d3DEb0e2E65061e8684eab21060123",
      },
    },
    // {
    //   name: "yvWETH-A Amount",
    //   inputType: InputType.ERC20Amount,
    //   description: "Amount to withdraw",
    //   validate: decimalValidator(18, BigInt(0)),
    //   options: {
    //     token: "0x305F25377d0a39091e99B975558b1bdfC3975654",
    //   },
    // },
  ],
  render: async (context: Context) => {
    const txs: Tx[] = [];

    const configs = [
      // {
      //   addr: "0x90b2f54C6aDDAD41b8f6c4fCCd555197BC0F773B",
      //   fromSymbol: "yvDAI-A",
      //   toSymbol: "DAI",
      //   decimals: 18,
      //   abi: yvDAI
      // },
      // {
      //   addr: "0xA013Fbd4b711f9ded6fB09C1c0d358E2FbC2EAA0",
      //   fromSymbol: "yvUSDC-A",
      //   toSymbol: "USDC.e",
      //   decimals: 6,
      //   abi: yvUSDC
      // },
      {
        addr: "0xBb287E6017d3DEb0e2E65061e8684eab21060123",
        fromSymbol: "yvUSDT-A",
        toSymbol: "USDT",
        decimals: 6,
        abi: yvUSDT
      },
      // {
      //   addr: "0x305F25377d0a39091e99B975558b1bdfC3975654",
      //   fromSymbol: "yvWETH-A",
      //   toSymbol: "WETH",
      //   decimals: 18,
      //   abi: yvWETH
      // },
    ];

    const client = createPublicClient({
      chain: context.chain,
      transport: http(),
    });

    for (let i = 0; i < configs.length; i++) {
      const config = configs[i];
      const contract = getContract({
        address: config.addr as Address,
        abi: config.abi,
        client: client
      });

      const fromAmount = parseUnits(context.inputs[i], config.decimals);
      if (fromAmount == 0n) continue;
      const pricePerSahre = await contract.read.pricePerShare() as bigint;
      const decimal = await contract.read.decimals() as number;
      const fixed = (Number(pricePerSahre) * parseFloat(context.inputs[i])).toFixed()
      console.log(`💥 fixed: ${fixed.toString()}`)
      const toAmount = BigInt(fixed);
      console.log(`💥 toAmount: ${toAmount.toString()}`)
      // const toAmount = pricePerSahre * parseUnits(context.inputs[i], decimal) / BigInt(10 ** decimal);

      txs.push({
        name: `Withdraw ${config.fromSymbol}`,
        description: `Withdraw ${context.inputs[i]} ${config.fromSymbol} to ${formatUnits(toAmount, config.decimals)} ${config.toSymbol}`,
        to: contract.address,
        value: 0n,
        data: encodeFunctionData({
          abi: contract.abi,
          functionName: "redeem",
          args: [
            fromAmount,
            context.account.address,
            context.account.address,
            1
          ],
        }),
        abi: contract.abi as Abi,
      });
    }

    return txs;
  }
};

export default [yearnWithdrawal];
