import yearnCases from "./yearn";
import etherfiCases from "./etherfi";
import eigenpie from "./eigenpie";
import zircuit from "./zircuit";
import juiceCases from "./juice";
import orbitCases from "./orbit";
import kelpAndPendleCases from "./kelp+pendle";
import pendlePointsCases from "./pendle-points";
import scrollCases from "./scroll";
import zircuitEthenaCases from "./zircuit-ethena";
import aerodromeCases from "./aerodrome";
import fomopolyCases from "./fomopoly";
import zircuitRenzoCases from "./zircuit-renzo";
import genesis from "./genesis";
import zircuitEtherfiCases from "./zircuit-etherfi";
import ethenaCases from "./ethena";
import eigenpieAndSwellCases from "./eigenpie+swell";
import { BatchCase } from "./types";

let cases: { [key: string]: BatchCase } = {};

const _allCases = [
  ...yearnCases,
  ...etherfiCases,
  ...eigenpie,
  ...zircuit,
  ...juiceCases,
  ...orbitCases,
  ...kelpAndPendleCases,
  ...pendlePointsCases,
  ...scrollCases,
  ...zircuitEthenaCases,
  ...aerodromeCases,
  ...fomopolyCases,
  ...zircuitRenzoCases,
  ...genesis,
  ...zircuitEtherfiCases,
  ...ethenaCases,
  ...eigenpieAndSwellCases,
];

for (let k in _allCases) {
  let c = _allCases[k];
  cases[c.id] = c;
}

export default cases;
