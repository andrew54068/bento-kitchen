import type { Abi, Address } from "abitype";
import {
  Context,
  BatchCase,
  InputType,
  Tag,
  Tx,
} from "@/models/cases/v3/types";
import { addressValidator, decimalValidator } from "@/models/cases/v3/utils";
import {
  encodeAbiParameters,
  createPublicClient,
  getContract,
  encodeFunctionData,
  parseUnits,
  http,
} from "viem";
import EETH from "./abi/EETH.json";
import EETHLiquidityPool from "./abi/EETHLiquidityPool.json";
import Inbox from "./abi/Inbox.json";
import L1GatewayRouter from "./abi/L1GatewayRouter.json";
import L1ERC20Gateway from "./abi/L1ERC20Gateway.json";
import WeETH from "./abi/WeETH.json";

const etherFiArbitrumBridge: BatchCase = {
  id: "ether_fi_arbitrum_bridge",
  name: "EtherFi Arbitrum Bridge",
  description:
    "This strategy helps you to stake and bridge weETH to arbtirum with just one click! This opens up opportunity of high yields and big point rewards on Arbitrum.",
  website: {
    title: "Ether.fi",
    url: "https://ether.fi",
  },
  tags: ["Ethereum", "LRT", "DeFi", "ETH", "weETH"].map(
    (name) => ({ title: name } as Tag)
  ),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: 1,
  atomic: true,
  renderExpiry: 15,
  inputs: [
    {
      name: "ETH Amount",
      inputType: InputType.NativeAmount,
      description:
        "Amount to stake and bridge (leave ~0.001 ETH for bridging fee)",
      validate: decimalValidator(18, BigInt(0)),
    },
    {
      name: "Target Address",
      inputType: InputType.Address,
      description:
        'Target address on Arbitrum (make sure your arbitrum address is consistent with "My Address" on Ethereum)',
      validate: addressValidator,
    },
  ],
  render: async (context: Context) => {
    let txs: Tx[] = [];
    const inputAmount = parseUnits(context.inputs[0], 18);

    const client = createPublicClient({
      chain: context.chain,
      transport: http(),
    });
    const eethLiqPoolAddr = "0x308861A430be4cce5502d0A12724771Fc6DaF216";
    const eETHAddr = "0x35fA164735182de50811E8e2E824cFb9B6118ac2";
    const weETHAddr = "0xCd5fE23C85820F7B72D0926FC9b05b43E359b7ee";
    const arbGatewayRouter = "0x72Ce9c846789fdB6fC1f34aC4AD25Dd9ef7031ef";
    const gatewayAddr = "0xa3A7B6F88361F48403514059F1F16C8E78d60EeC";
    const inboxAddr = "0x5aed5f8a1e3607476f1f81c3d8fe126deb0afe94";

    const eethLiqPool = getContract({
      address: eethLiqPoolAddr,
      abi: EETHLiquidityPool,
      client: client,
    });

    txs.push({
      name: "Stake eETH",
      description: "Stake ETH into ether.fi for eETH",
      to: eethLiqPoolAddr,
      value: inputAmount,
      data: encodeFunctionData({
        abi: EETHLiquidityPool,
        functionName: "deposit",
      }),
      abi: EETHLiquidityPool as Abi,
    });
    const { result } = await eethLiqPool.simulate.deposit([], {
      value: inputAmount,
    });
    const weETHAmount = result;
    console.log("weETH:", weETHAmount);
    const eETHAmount = await eethLiqPool.read.amountForShare([weETHAmount]);
    console.log("eETH:", eETHAmount);
    console.log("weETH: ", weETHAmount);

    // approve
    const eETH = getContract({
      address: eETHAddr,
      abi: EETH,
      client: client,
    });
    txs.push({
      name: "Approve eETH",
      description: "Approve eETH for wrapping",
      to: eETHAddr,
      value: 0n,
      data: encodeFunctionData({
        abi: EETH,
        functionName: "approve",
        args: [
          weETHAddr, // address
          eETHAmount, // assets
        ],
      }),
      abi: EETH as Abi,
    });

    // wrap
    txs.push({
      name: "Wrap eETH",
      description: "Wrap eETH into weETH",
      to: weETHAddr,
      value: 0n,
      data: encodeFunctionData({
        abi: WeETH,
        functionName: "wrap",
        args: [
          eETHAmount, // assets
        ],
      }),
      abi: WeETH as Abi,
    });

    txs.push({
      name: "Approve weETH",
      description: "Approve weETH for bridging",
      to: weETHAddr,
      value: 0n,
      data: encodeFunctionData({
        abi: WeETH,
        functionName: "approve",
        args: [
          gatewayAddr,
          weETHAmount, // assets
        ],
      }),
      abi: WeETH as Abi,
    });

    // bridge
    const arbAddr = context.inputs[1];
    const gasLimit = 200000n;
    const maxFeePerGas = 300000000n;

    const gateway = getContract({
      address: gatewayAddr,
      abi: L1ERC20Gateway,
      client: client,
    });
    const outboundCalldata = await gateway.read.getOutboundCalldata([
      weETHAddr, // token
      arbGatewayRouter, // from
      arbAddr, // to
      weETHAmount, // amount
      "0x", // calldata,
    ]);

    const inbox = getContract({
      address: inboxAddr,
      abi: Inbox,
      client: client,
    });
    const submissionFee = await inbox.read.calculateRetryableSubmissionFee([
      (outboundCalldata as string).length / 2, // data length
      0, // base fee
    ]);
    console.log("submission fee", submissionFee, submissionFee as bigint);

    const maxSubmissionCost = ((submissionFee as bigint) * 12n) / 10n;
    console.log("max submission cost", maxSubmissionCost);

    const calldata = encodeAbiParameters(
      [
        { name: "x", type: "uint256" }, // max submission cost
        { name: "y", type: "bytes" }, // data
      ],
      [maxSubmissionCost, "0x"]
    );

    txs.push({
      name: "Bridge weETH",
      description: "Bridge weETH into Arbitrum One with official bridge",
      to: arbGatewayRouter,
      value: gasLimit * maxFeePerGas + maxSubmissionCost,
      data: encodeFunctionData({
        abi: L1GatewayRouter,
        functionName: "outboundTransfer",
        args: [
          weETHAddr, // token
          arbAddr, // to
          weETHAmount - 1n, // amount, minus 1 for temp fix
          gasLimit, // max gas
          maxFeePerGas, // gas price bid
          calldata, // _data = ()
        ],
      }),
      abi: L1GatewayRouter as Abi,
    });

    return txs;
  },
};

export default [etherFiArbitrumBridge];
