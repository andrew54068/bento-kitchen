import type { Abi, Address } from "abitype";
import {
  Context,
  BatchCase,
  InputType,
  Tag,
  Tx,
} from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import { ReferalAccount } from "@/models/cases/v3/constants";
import {
  encodeFunctionData,
  parseUnits,
} from "viem";
import RswETH from "./abi/RswETH.json";
import ZircuitPool from "./abi/ZircuitPool.json";



const zircuitRswETH: BatchCase = {
  id: "zircuit_rsweth",
  name: "Zircuit with Swell rswETH restaking",
  description:
    "Deposit ETH to Farm 3 type Points with staking and restaking yield <BR> \
    step 1. Deposit ETH into Swell restaking smart contract to get $rswETH <BR> \
    step 2. Approve $rswETH to Zircuit deposit contract <BR> \
    step 3. Deposit $rswETH into Zircuit",
  website: {
    title: "Zircuit",
    url: "https://stake.zircuit.com/",
  },
  tags: ["Ethereum", "LRT", "DeFi", "ETH", "rswETH"].map(
    (name) => {
      if (name === "rswETH") {
        return { title: name, url: "https://app.swellnetwork.io/restake"  } as Tag
      }
      return { title: name } as Tag
    }
  ),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: 1,
  atomic: true,
  renderExpiry: 15,
  inputs: [
    {
      name: "ETH Amount",
      inputType: InputType.NativeAmount,
      description:
        "Amount to stake",
      validate: decimalValidator(18, BigInt(0)),
    },
  ],
  render: async (context: Context) => {
    let txs: Tx[] = [];
  
    const v = parseUnits(context.inputs[0], 18);
  
    const rswETHAddr = "0xFAe103DC9cf190eD75350761e95403b7b8aFa6c0";
    const zircuitAddr = "0xF047ab4c75cebf0eB9ed34Ae2c186f3611aEAfa6";

    const estimateRSWETHAmount = v * BigInt(992) / BigInt(1000);
   

    txs.push({
      name: "Wrap to rswETH",
      description: "Wrap ETH to rswETH",
      to: rswETHAddr,
      value: v,
      data: encodeFunctionData({
        abi: RswETH,
        functionName: 'depositWithReferral',
        args:[ReferalAccount]
      }),
      abi: RswETH as Abi,
    })

    txs.push({
      name: "Approve rswETH",
      description: "approve rswETH to Zircuit",
      to: rswETHAddr,
      value: 0n,
      data: encodeFunctionData({
        abi: RswETH,
        functionName: 'approve',
        args:[
          zircuitAddr,
          estimateRSWETHAmount,
        ]
      }),
      abi: RswETH as Abi,
    })

    txs.push({
      name: "Deposit rswETH",
      description: "deposit rswETH to Zircuit",
      to: zircuitAddr,
      value: 0n,
      data: encodeFunctionData({
        abi: ZircuitPool,
        functionName: 'depositFor',
        args:[
          rswETHAddr,
          context.account.address,
          estimateRSWETHAmount,
        ]
      }),
      abi: ZircuitPool as Abi,
    })

    return txs;
  },
};

export default [zircuitRswETH];
