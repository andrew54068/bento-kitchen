import type { Abi } from "abitype";
import {
  Context,
  BatchCase,
  InputType,
  Tag,
  Tx,
} from "@/models/cases/v3/types";
import { decimalValidator, integerValidator } from "@/models/cases/v3/utils";
import { encodeFunctionData, parseUnits } from "viem";
import Fomopoly from "./abi/fomopoly.json";

const fomopoly: BatchCase = {
  id: "fomopoly_batch_buy",
  name: "Batch Roll Dice and Buy Lands",
  description: "Help you aquire lands in Fomopoly in one go",
  website: {
    title: "Fomopoly",
    url: "https://fomopoly.xyz/",
  },
  tags: ["GameFi", "Blast", "FMP"].map((name) => ({ title: name } as Tag)),
  curatorTwitter: {
    name: "Fomopoly - Powered by FreeFlow",
    url: "https://twitter.com/Fomopoly_Game",
  },
  networkId: 81457,
  atomic: true,
  renderExpiry: undefined,
  inputs: [
    {
      name: "Budget",
      inputType: InputType.NativeAmount,
      description: "Budget to buy lands",
      validate: decimalValidator(18, BigInt(0)),
    },
    {
      name: "Rounds Amount",
      inputType: InputType.Text,
      description: "How many rounds you wish to batch?",
      validate: integerValidator,
    },
  ],
  render: async (context: Context) => {
    let txs: Tx[] = [];
    const buyLandBudget = parseUnits(context.inputs[0], 18);

    // Addresses
    const fomopolyProxyContract = "0xaC207Fc2F22BD8e292e20B7a0d1c2ACCf04C0609";

    const rounds = +context.inputs[1];

    for (let i = 0; i < rounds; i++) {
      txs.push({
        name: `Roll dice`,
        description: `Roll dice for round ${i + 1}`,
        to: fomopolyProxyContract,
        value: BigInt(0),
        data: encodeFunctionData({
          abi: Fomopoly,
          functionName: "move",
          args: [0],
        }),
        abi: Fomopoly as Abi,
      });

      txs.push({
        name: `Buy land`,
        description: `Buy lands in round ${i + 1}`,
        to: fomopolyProxyContract,
        value: buyLandBudget,
        data: encodeFunctionData({
          abi: Fomopoly,
          functionName: "buyLand",
          args: [],
        }),
        abi: Fomopoly as Abi,
      });
    }

    return txs;
  },
};

export default [fomopoly];
