import type { Abi } from "abitype";
import {
    Context,
    BatchCase,
    InputType,
    Tag,
    Tx,
} from "@/models/cases/v3/types";
import { addressValidator, decimalValidator } from "@/models/cases/v3/utils";
import {
    createPublicClient,
    getContract,
    encodeFunctionData,
    parseUnits,
    http,
} from "viem";

import WETH from "./abi/WETH.json";
import DEGEN from "./abi/DEGEN.json";
import AerodromeRouter from "./abi/AerodromeRouter.json";
import vAMMWETHDEGEN from "./abi/vAMMWETHDEGEN.json";
import AerodromeGauge from "./abi/AerodromeGauge.json";

// adresses on Base
const wETHAddr = "0x4200000000000000000000000000000000000006";
const degenAddr = "0x4ed4E862860beD51a9570b96d89aF5E1B0Efefed";
const aerodromeRouterAddr = "0xcF77a3Ba9A5CA399B7c97c74d54e5b1Beb874E43";
const vAMMWETHDEGENAddr = "0x2C4909355b0C036840819484c3A882A95659aBf3";
const aerodromeGaugeAddr = "0x86a1260ab9f758026ce1a5830bdff66dbcf736d5";

const aerodromeDegenLiquidity: BatchCase = {
    id: "aerodrome_degen_liquidity",
    name: "Provide DEGEN Liquidity on Aerodrome",
    description: "Provide DEGEN Liquidity on Aerodrome",
    website: {
        title: "Aerodrome",
        url: "https://aerodrome.finance/"
    },
    tags: ["Base", "DEGEN", "Aerodrome", "ETH"].map(
        (name) => ({ title: name } as Tag)
    ),
    networkId: 8453,
    atomic: true,
    inputs: [
        {
            name: "ETH Amount",
            inputType: InputType.NativeAmount,
            description: "Amount desired to provide liquidity",
            validate: decimalValidator(18, BigInt(0)),
        },
        {
            name: "DEGEN amount",
            inputType: InputType.ERC20Amount,
            description: "Amount desired to provide liquidity",
            validate: decimalValidator(18, BigInt(0)),
            options: {
                token: degenAddr,
            },
        }
    ],
    render: async (context: Context) => {
        const inputETHAmount = parseUnits(context.inputs[0], 18);
        const inputDegenAmount = parseUnits(context.inputs[1], 18);

        // simulate add liqudity to estimate the amount of ETH to deposit
        const client = createPublicClient({
            chain: context.chain,
            transport: http(),
        });
        const aerodromeRouter = getContract({
            address: aerodromeRouterAddr,
            abi: AerodromeRouter,
            client: client,
        });
        const { result: factoryAddr } = await aerodromeRouter.simulate.defaultFactory([],{ value: 0n });
        const { result: quotes } = await aerodromeRouter.simulate.quoteAddLiquidity(
            [
                wETHAddr, // tokenA
                degenAddr, // tokenB
                false, // stable
                factoryAddr, // _factory
                inputETHAmount, // amountDesiredA
                inputDegenAmount, // anountDesiredB
            ], 
            { value: 0n }
        );
        const ethAmountToDeposit = quotes[0];
        const degenAmountToDeposit = quotes[1];
        const estimatedLiquidity = quotes[2];

        let txs: Tx[] = [];
        
        // wrap ETH
        txs.push({
            name: "Wrap ETH",
            description: "Wrap ETH",
            to: wETHAddr,
            value: ethAmountToDeposit,
            data: encodeFunctionData({
                abi: WETH,
                functionName: "deposit",
            }),
            abi: WETH as Abi,
        });

        // approve WETH
        txs.push({
            name: "Approve WETH",
            description: "Approve WETH transfer for providing liquidity",
            to: wETHAddr,
            value: 0n,
            data: encodeFunctionData({
                abi: WETH,
                functionName: "approve",
                args: [
                    aerodromeRouterAddr, // guy
                    ethAmountToDeposit, // wad
                ],
            }),
            abi: WETH as Abi,
        });

        // approve DEGEN
        txs.push({
            name: "Approve DEGEN",
            description: "Approve DEGEN transfer for providing liquidity",
            to: degenAddr,
            value: 0n,
            data: encodeFunctionData({
                abi: DEGEN,
                functionName: "approve",
                args: [
                    aerodromeRouterAddr, // spender
                    degenAmountToDeposit, // value
                ],
            }),
            abi: DEGEN as Abi,
        });

        // add WETH and DEGEN liqudity
        const currentTimeStamp = Math.floor(Date.now() / 1000);
        const deadline = currentTimeStamp + 60 * 3;
        txs.push({
            name: "Provide liquidity",
            description: "Provide WETH and DEGEN liquidity on Aerodrome",
            to: aerodromeRouterAddr,
            value: 0n,
            data: encodeFunctionData({
                abi: AerodromeRouter,
                functionName: "addLiquidity",
                args: [
                    wETHAddr, // tokenA
                    degenAddr, // tokenB
                    false, // stable
                    ethAmountToDeposit, // amountADesired
                    degenAmountToDeposit, // amountBDesired
                    ethAmountToDeposit * BigInt(992) / BigInt(1000), // amountAMin
                    degenAmountToDeposit * BigInt(992) / BigInt(1000), // aountBMin
                    context.account.address, // to
                    deadline, // deadline
                ],
            }),
            abi: AerodromeRouter as Abi,
        });

        // approve vAMMWETHDEGENAddr
        txs.push({
            name: "Approve LP Token(vAMM-WETH/DEGEN)",
            description: "Approve LP Token(vAMM-WETH/DEGEN) for farming",
            to: vAMMWETHDEGENAddr,
            value: 0n,
            data: encodeFunctionData({
                abi: vAMMWETHDEGEN,
                functionName: "approve",
                args: [
                    aerodromeGaugeAddr, // spender
                    estimatedLiquidity // amount
                ],
            }),
            abi: vAMMWETHDEGEN as Abi,
        });

        // deposit LP token
        txs.push({
            name: "Deposit LP token(vAMM-WETH/DEGEN)",
            description: "Deposit LP token(vAMM-WETH/DEGEN) and start farming",
            to: aerodromeGaugeAddr,
            value: 0n,
            data: encodeFunctionData({
                abi: AerodromeGauge,
                functionName: "deposit",
                args: [
                    estimatedLiquidity * BigInt(998) / BigInt(1000) // _amount
                ],
            }),
            abi: AerodromeGauge as Abi,
        });

        return txs;
    },
}

export default [aerodromeDegenLiquidity];