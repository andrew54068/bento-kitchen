Orbit Points

## Overview

Orbit is a lending protocol that allows users to supply assets and borrow assets. In this case, the user supplies ETH to the contract, uses the `oETH` token as collateral, and borrows ETH from the contract.

[Transaction](https://blastexplorer.io/tx/0xc9db07ea12e2595bcacb411bf2b9aa7b19cc6f366c4d5880cc921871c674c567)

## Steps

#### Supply ETH

The user supplies ETH to the contract by minting a `oETH` token.

func: `mint` (`0x1249c58b`)
params: none

#### Use ETH as collateral

The user uses the `oETH` token as collateral.

func: `enterMarkets` (`0xc2998238`)
params: oTokens: `address[]`

#### Borrow ETH

The user borrows ETH from the contract.

func: `borrow` (`0xc5ebeaec`)
params: borrowAmount: `uint256`

## References

- [Orbit](https://app.orbitlending.io/)
