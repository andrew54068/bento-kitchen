import type { Abi } from "abitype";
import { Context, BatchCase, InputType, Tag, Tx } from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import { getContract, formatEther, createPublicClient, encodeFunctionData, toHex, pad, parseUnits, http } from "viem";
import OETH from "./abi/OETH.json";
import Comptroller from "./abi/OrbitComptroller.json";

const orbit: BatchCase = {
  id: "orbit_points",
  name: "Earn Points with Orbit",
  description: "Supply & Borrow on Orbit to earn Blast Points + Blast Gold + $ORBIT",
  website: {
    title: "Orbit",
    url: "https://app.orbitlending.io/",
  },
  tags: ["DeFi", "ETH", "WETH"].map((name) => ({ title: name } as Tag)),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: 81457,
  atomic: true,
  renderExpiry: undefined,
  inputs: [
    {
      name: "ETH Amount",
      inputType: InputType.NativeAmount,
      description: "Amount to deposit",
      validate: decimalValidator(18, BigInt(0)),
    },
    {
      name: "Iterations (0 - 10, default: 1)",
      inputType: InputType.Text,
      description: "Number of iterations to run",
    },
  ],
  render: async (context: Context) => {
    let txs: Tx[] = [];
    const ethInputAmount = parseUnits(context.inputs[0], 18);

    // Addresses
    const oETHAddr = "0x0872b71EFC37CB8DdE22B2118De3d800427fdba0"; // oTOKEN address
    const comptrollerProxyAddr = "0x1E18C3cb491D908241D0db14b081B51be7B6e652"; // comptroller proxy address

    // Config
    const borrowRate = 65n; // borrow rate. 80% is the safe limit. 65% is the recommended.
    const iterationsCount = context.inputs[1] && parseInt(context.inputs[1]) > 0 ? parseInt(context.inputs[1]) : 1;
    // Set initial supply & borrow amount
    let supplyAmount = ethInputAmount;
    let borrowAmount = (supplyAmount * borrowRate) / 100n;

    const client = createPublicClient({
      chain: context.chain,
      transport: http(),
    });

    const oETH = getContract({
      address: oETHAddr,
      abi: OETH,
      client: client,
    });

    const comptroller = getContract({
      address: comptrollerProxyAddr,
      abi: Comptroller,
      client: client,
    });

    // Check if user has entered the market
    const { result: hasEntered } = await comptroller.simulate.checkMembership([context.account.address, oETH.address]);

    // Enter market if not entered
    if (!hasEntered) {
      txs.push({
        name: "Use oETH as Collateral",
        description: `Enter market with oETH`,
        to: comptroller.address,
        value: 0n,
        data: encodeFunctionData({
          abi: comptroller.abi,
          functionName: "enterMarkets",
          args: [[oETH.address]],
        }),
        abi: comptroller.abi as Abi,
      });
    }

    // Generate transactions for each iteration
    for (let index = 0; index < iterationsCount; index++) {
      // Mint oETH
      txs.push({
        name: `Supply ETH ${iterationsCount == 1 ? "" : `- #${index + 1}`}`,
        description: `Mint oETH with ${index === 0 ? "initial" : "borrowed"} ${formatEther(supplyAmount)} ETH`,
        to: oETH.address,
        value: supplyAmount,
        data: encodeFunctionData({
          abi: oETH.abi,
          functionName: "mint",
        }),
        abi: oETH.abi as Abi,
      });

      // Calculate borrow amount
      borrowAmount = (supplyAmount * borrowRate) / 100n;

      // Borrow ETH
      txs.push({
        name: `Borrow ETH ${iterationsCount == 1 ? "" : `- #${index + 1}`}`,
        description: `Borrow ${borrowRate}% ETH`,
        to: oETH.address,
        value: 0n,
        data: encodeFunctionData({
          abi: oETH.abi,
          functionName: "borrow",
          args: [borrowAmount],
        }),
        abi: oETH.abi as Abi,
      });

      // Update supplyAmount for next iteration
      supplyAmount = borrowAmount;
    }

    return txs;
  },
};

export default [orbit];
