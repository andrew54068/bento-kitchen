import type { Abi } from "abitype";
import {
    Context,
    BatchCase,
    InputType,
    Tag,
    Tx,
} from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import {
    createPublicClient,
    getContract,
    encodeFunctionData,
    parseUnits,
    http,
} from "viem";

import StaderStakePoolsManager from "./abi/StaderStakePoolsManager.json";
import ETHx from "./abi/ETHx.json";
import LRTDepositPool from "./abi/LRTDepositPool.json";
import RSETH from "./abi/rsETH.json";
import ActionAddRemoveLiqV3 from "./abi/ActionAddRemoveLiqV3.json";

const fetchJSON = async (request: string | URL | Request) => {
    try {
      const response = await fetch(request);
      const contentType = response.headers.get("content-type");
      if (!contentType || !contentType.includes("application/json")) {
        throw new TypeError("Oops, we haven't got JSON!");
      }
      const jsonData = await response.json();
      return jsonData;
      // process your data further
    } catch (error) {
      console.error("Error:", error);
    }
    return null;
};

const kelpAndPendle: BatchCase = {
    id: "kelp_and_pendle",
    name: "Restake ETH on KelpDAO and Farm on Pendle",
    description: "Restake ETH on KelpDAO and Farm on Pendle",
    website: {
        title: "KelpDAO",
        url: "https://kelpdao.xyz/"
    },
    tags: ["Ethereum", "ETH", "KelpDAO", "Pendle", "Stader", "Restake"].map(
        (name) => ({ title: name } as Tag)
    ),
    networkId: 1,
    atomic: true,
    inputs: [
        {
            name: "ETH Amount",
            inputType: InputType.NativeAmount,
            description:
              "Amount to stake and farm (leave ~0.001 ETH for gas fee)",
            validate: decimalValidator(18, BigInt(0)),
        },
    ],
    render: async (context: Context) => {
        const inputAmount = parseUnits(context.inputs[0], 18);

        const staderStakingPoolManagerAddr = "0xcf5EA1b38380f6aF39068375516Daf40Ed70D299";
        const ethxAddr = "0xA35b1B31Ce002FBF2058D22F30f95D405200A15b";
        const lrtDepositPoolAddr = "0x036676389e48133B63a802f8635AD39E752D375D";
        const rsETHAddr = "0xA1290d69c65A6Fe4DF752f95823fae25cB99e5A7";
        const pendleRouterV3Addr = "0x00000000005BBB0EF59571E58418F9a4357b68A0";
        const pendleRSETHMarketAddr = "0x4f43c77872Db6BA177c270986CD30c3381AF37Ee";

        const kelpReferralId = "0xa9857dbaa6f99e5a557b23d321843a48c5a5417029894f76db4c80bddab116bc";

        const client = createPublicClient({
            chain: context.chain,
            transport: http(),
        });

        let txs: Tx[] = [];

        // stake ETH for ETHx
        txs.push({
            name: "Stake ETH",
            description: "Stake ETH for ETHx on Stader",
            to: staderStakingPoolManagerAddr,
            value: inputAmount,
            data: encodeFunctionData({
                abi: StaderStakePoolsManager,
                functionName: "deposit",
                args: [
                    context.account.address, // _receiver
                ],
            }),
            abi: StaderStakePoolsManager as Abi,
        });

        const staderStakingPoolManager = getContract({
            address: staderStakingPoolManagerAddr,
            abi: StaderStakePoolsManager,
            client: client
        });

        const { result: ethxAmount } = await staderStakingPoolManager.simulate.deposit([
            context.account.address,
        ], { 
            value: inputAmount,
        });

        // aprove ETHx transfer
        txs.push({
            name: "Approve ETHx",
            description: "Approve ETHx transfer for swapping ETHx to rsETH",
            to: ethxAddr,
            value: 0n,
            data: encodeFunctionData({
                abi: ETHx,
                functionName: "approve",
                args: [
                    lrtDepositPoolAddr, // spender
                    ethxAmount // amount
                ],
            }),
            abi: ETHx as Abi,
        });

        // deposit ETHx for rsETH
        const ethxAmountToDeposit = ethxAmount * BigInt(998) / BigInt(1000);
        txs.push({
            name: "Deposit ETHx",
            description: "Deposit ETHx for rsETH",
            to: lrtDepositPoolAddr,
            value: 0n,
            data: encodeFunctionData({
                abi: LRTDepositPool,
                functionName: "depositAsset",
                args: [
                    ethxAddr, // asset
                    ethxAmountToDeposit, // despositAmount
                    0n, // minRSETHAmountExpected
                    kelpReferralId, // referralId
                ],
            }),
            abi: LRTDepositPool as Abi,
        });

        const lrtDepositPool = getContract({
            address: lrtDepositPoolAddr,
            abi: LRTDepositPool,
            client: client
        });
        const { result: rsETHAmount } = await lrtDepositPool.simulate.getRsETHAmountToMint([
            ethxAddr,
            ethxAmountToDeposit
        ], {
            value: 0n,
        });
        
        // approve rsETH
        txs.push({
            name: "Approve rsETH",
            description: "Approve rsETH transfer for farming on Pendle",
            to: rsETHAddr,
            value: 0n,
            data: encodeFunctionData({
                abi: RSETH,
                functionName: "approve",
                args: [
                    pendleRouterV3Addr, // spender
                    rsETHAmount // amount
                ]
            }),
            abi: RSETH as Abi
        });

        // farming on Pendle
        const rsETHAmountToDeposit = rsETHAmount * BigInt(998) / BigInt(1000);
        const ethereumChainId = "1";
        const syTokenInOut = await fetchJSON(
            "https://api-v2.pendle.finance/sdk/api/v1/syTokenInOut?" +
              new URLSearchParams({
                chainId: ethereumChainId,
                marketAddr: pendleRSETHMarketAddr,
              })
        );
        
        const transactionInfo = await fetchJSON(
            "https://api-v2.pendle.finance/sdk/api/v1/addLiquiditySingleToken?" +
              new URLSearchParams({
                chainId: ethereumChainId,
                receiverAddr: context.account.address || "",
                marketAddr: pendleRSETHMarketAddr,
                tokenInAddr: rsETHAddr,
                syTokenInAddr: syTokenInOut.outputTokens[0],
                amountTokenIn: rsETHAmountToDeposit.toString(),
                slippage: "0.002",
              })
        );
        txs.push({
            name: "Provide PT/SY Liquidity on Pendle",
            description:
              "Provide Liquidity to PT/SY pool of Pendle Finance on Ethereum",
            to: transactionInfo.transaction.to,
            value: 0n,
            data: transactionInfo.transaction.data,
            abi: ActionAddRemoveLiqV3 as Abi
          });

        return txs;
    },
} 

export default [kelpAndPendle];
