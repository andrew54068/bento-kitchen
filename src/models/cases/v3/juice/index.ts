import type { Abi } from "abitype";
import { Context, BatchCase, InputType, Tag, Tx } from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import { getContract, formatEther, createPublicClient, encodeFunctionData, toHex, pad, parseUnits, http } from "viem";
import WETH from "./abi/WETH.json";
import JuiceVault from "./abi/JuiceVault.json";
import JuiceAccount from "./abi/JuiceAccount.json";

const juice: BatchCase = {
  id: "juice_points",
  name: "Earn Points with Juice",
  description: "Earn Blast + Eigen Layer + Renzo + Juice + Thruster Points",
  website: {
    title: "Juice",
    url: "https://www.juice.finance/",
  },
  tags: ["DeFi", "ETH", "WETH"].map((name) => ({ title: name } as Tag)),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: 81457,
  atomic: true,
  renderExpiry: undefined,
  inputs: [
    {
      name: "ETH Amount",
      inputType: InputType.NativeAmount,
      description: "Amount to deposit",
      validate: decimalValidator(18, BigInt(0)),
    },
  ],
  render: async (context: Context) => {
    let txs: Tx[] = [];
    const ethInputAmount = parseUnits(context.inputs[0], 18);

    // Addresses
    const wETHAddr = "0x4300000000000000000000000000000000000004";
    const juiceVaultAddr = "0x23eBa06981B5c2a6f1a985BdCE41BD64D18e6dFA"; // juice finance wETH collateral vault
    const juiceThrusterV3StrategyAddr = "0x741011f52B7499ca951f8b8Ee547DD3Cdd813Fda";

    // Config
    const borrowRate = 200n; // borrow rate. 297% is the safe limit.
    const slippage = 96n; // uniswap slippage for min out amount

    const client = createPublicClient({
      chain: context.chain,
      transport: http(),
    });

    const wETH = getContract({
      address: wETHAddr,
      abi: WETH,
      client: client,
    });

    const juiceVault = getContract({
      address: juiceVaultAddr,
      abi: JuiceVault,
      client: client,
    });

    // Wrap ETH
    txs.push({
      name: "Wrap ETH",
      description: `Wrap ${context.inputs[0]} ETH to wETH`,
      to: wETH.address,
      value: ethInputAmount,
      data: encodeFunctionData({
        abi: wETH.abi,
        functionName: "deposit",
      }),
      abi: wETH.abi as Abi,
    });

    // Check if account has created
    const { result: juiceAccountAddr } = await juiceVault.simulate.getAccount([context.account.address]);

    const isUncreated =
      (await client.getBytecode({
        address: juiceAccountAddr,
      })) === undefined;

    // Create account if uncreated
    if (isUncreated) {
      txs.push({
        name: "Create Account",
        description: "Create a Juice account",
        to: juiceVault.address,
        value: 0n,
        data: encodeFunctionData({
          abi: juiceVault.abi,
          functionName: "createAccount",
          args: undefined,
        }),
        abi: juiceVault.abi as Abi,
      });
    }

    // Approve wETH
    txs.push({
      name: "Approve wETH",
      description: `Approve ${context.inputs[0]} wETH for Juice`,
      to: wETH.address,
      value: 0n,
      data: encodeFunctionData({
        abi: wETH.abi,
        functionName: "approve",
        args: [juiceVault.address, ethInputAmount],
      }),
      abi: wETH.abi as Abi,
    });

    // Deposit wETH
    txs.push({
      name: "Deposit wETH",
      description: `Deposit ${context.inputs[0]} wETH to Juice`,
      to: juiceVault.address,
      value: 0n,
      data: encodeFunctionData({
        abi: juiceVault.abi,
        functionName: "deposit",
        args: [ethInputAmount, context.account.address],
      }),
      abi: juiceVault.abi as Abi,
    });

    // Borrow wETH
    const borrowAmount = (ethInputAmount * borrowRate) / 100n;
    txs.push({
      name: "Borrow wETH",
      description: `Borrow ${borrowRate}% wETH from Juice`,
      to: juiceAccountAddr,
      value: 0n,
      data: encodeFunctionData({
        abi: JuiceAccount,
        functionName: "borrow",
        args: [borrowAmount],
      }),
      abi: JuiceAccount as Abi,
    });

    // Deposit wETH to Juice Thruster V3 Strategy
    // https://app.juice.finance/vaults/0x741011f52B7499ca951f8b8Ee547DD3Cdd813Fda
    const minOutAmount = (borrowAmount * slippage) / 100n;
    txs.push({
      name: "Deposit borrowed wETH to Juice ezETH Spot Long vault",
      description: `Deposit ${formatEther(borrowAmount)} wETH to Juice ezETH Spot Long vault`,
      to: juiceAccountAddr,
      value: 0n,
      data: encodeFunctionData({
        abi: JuiceAccount,
        functionName: "strategyDeposit",
        args: [juiceThrusterV3StrategyAddr, borrowAmount, pad(toHex(minOutAmount))],
      }),
      abi: JuiceAccount as Abi,
    });

    return txs;
  },
};

export default [juice];
