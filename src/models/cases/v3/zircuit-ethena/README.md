Zircuit x Ethena

## Sample Transaction

- [USDC case](https://etherscan.io/tx/0xfbd6febdb0cef9fe2abae0c1f8316029bffb3c8fad7031e2d04361edcd3db2a7)
- [USDT case](https://etherscan.io/tx/0xf429cbf740e38b455cd4065f33276c34f0e783e0646361bd80149e7fda0bc215)

## References

- [Zircuit](https://stake.zircuit.com/)
- [Ethena](https://app.ethena.fi/)
