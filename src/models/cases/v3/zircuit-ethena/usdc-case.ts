import type { Abi } from "abitype";
import { Context, BatchCase, InputType, Tag, Tx } from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import { createPublicClient, getContract, http, encodeFunctionData, parseUnits, formatUnits } from "viem";
import USDC from "./abi/USDC.json";
import USDe from "./abi/USDe.json";
import CurveStableSwap from "./abi/CurveStableSwap.json";
import ZircuitRestakingPool from "./abi/ZircuitRestakingPool.json";

const usdcCase: BatchCase = {
  id: "zircuit_ethena_usdc",
  name: "Earn points on Zircuit using USDC",
  description: "Swap USDC to USDe on Curve and stake to Zircuit",
  website: {
    title: "Zircuit",
    url: "https://www.zircuit.com/",
  },
  tags: ["DeFi", "USDC", "USDe", "Points", "Curve"].map((name) => ({ title: name } as Tag)),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: 1,
  atomic: true,
  renderExpiry: undefined,
  inputs: [
    {
      name: "USDC Amount",
      inputType: InputType.ERC20Amount,
      description: "Amount to buy USDe",
      validate: decimalValidator(6, BigInt(0)),
      options: {
        token: "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48",
      },
    },
  ],
  render: async (context: Context) => {
    const txs: Tx[] = [];

    const usdcAddr = "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48";
    const usdeAddr = "0x4c9edd5852cd905f086c759e8383e09bff1e68b3";
    const curveStableSwapAddr = "0x02950460E2b9529D0E00284A5fA2d7bDF3fA4d72";
    const zircuitRestakingPoolAddr = "0xF047ab4c75cebf0eB9ed34Ae2c186f3611aEAfa6";

    const client = createPublicClient({
      chain: context.chain,
      transport: http(),
    });

    const usdc = getContract({
      address: usdcAddr,
      abi: USDC,
      client: client,
    });

    const usde = getContract({
      address: usdeAddr,
      abi: USDe,
      client: client,
    });

    const curveStableSwap = getContract({
      address: curveStableSwapAddr,
      abi: CurveStableSwap,
      client: client,
    });

    const zircuitRestakingPool = getContract({
      address: zircuitRestakingPoolAddr,
      abi: ZircuitRestakingPool,
      client: client,
    });

    const usdcAllowance = await usdc.read.allowance([
      context.account.address,
      curveStableSwap.address,
    ]) as bigint;

    const usdcAmount = parseUnits(context.inputs[0], 6);

    if (usdcAmount > usdcAllowance) {
      txs.push({
        name: `Approve USDC`,
        description: `Approve ${context.inputs[0]} USDC for Curve`,
        to: usdc.address,
        value: 0n,
        data: encodeFunctionData({
          abi: usdc.abi,
          functionName: "approve",
          args: [curveStableSwap.address, usdcAmount],
        }),
        abi: usdc.abi as Abi,
      });
    }

    const usdeAmount = await curveStableSwap.read.get_dy([
      "1", // coins index of USDC
      "0", // coins index of USDe
      usdcAmount
    ]) as bigint;
    
    const usdeFormatAmount = formatUnits(usdeAmount, 18);

    txs.push({
      name: `Buy USDe`,
      description: `Swap ${context.inputs[0]} USDC to ${usdeFormatAmount} USDe`,
      to: curveStableSwap.address,
      value: 0n,
      data: encodeFunctionData({
        abi: curveStableSwap.abi,
        functionName: "exchange",
        args: [
          "1", // coins index of USDC
          "0", // coins index of USDe
          usdcAmount,
          usdeAmount,
        ],
      }),
      abi: curveStableSwap.abi as Abi,
    });

    const usdeAllowance = await usde.read.allowance([
      context.account.address,
      zircuitRestakingPoolAddr,
    ]) as bigint;

    if (usdeAmount > usdeAllowance) {
      txs.push({
        name: `Approve USDe`,
        description: `Approve ${usdeFormatAmount} USDe for Zircuit`,
        to: usde.address,
        value: 0n,
        data: encodeFunctionData({
          abi: usde.abi,
          functionName: "approve",
          args: [zircuitRestakingPoolAddr, usdeAmount],
        }),
        abi: usde.abi as Abi,
      });
    }

    txs.push({
      name: `Stake USDe`,
      description: `Stake ${usdeFormatAmount} USDe to Zircuit`,
      to: zircuitRestakingPool.address,
      value: 0n,
      data: encodeFunctionData({
        abi: zircuitRestakingPool.abi,
        functionName: "depositFor",
        args: [
          usdeAddr,
          context.account.address,
          usdeAmount,
        ],
      }),
      abi: zircuitRestakingPool.abi as Abi,
    });

    return txs;
  }
};

export default usdcCase;
