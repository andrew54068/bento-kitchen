import usdcCase from "./usdc-case";
import usdtCase from "./usdt-case";

const zircuitEthenaCases = [
  usdcCase,
  usdtCase,
];

export default zircuitEthenaCases;
