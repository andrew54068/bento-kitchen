import type { Abi } from "abitype";
import { Context, BatchCase, InputType, Tag, Tx } from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import { createPublicClient, getContract, http, encodeFunctionData, parseUnits, formatUnits } from "viem";
import USDT from "./abi/USDT.json";
import USDe from "./abi/USDe.json";
import UniswapRouterV3 from "./abi/UniswapRouterV3.json";
import UniswapQuoterV3 from "./abi/UniswapQuoterV3.json";
import ZircuitRestakingPool from "./abi/ZircuitRestakingPool.json";

const usdtCase: BatchCase = {
  id: "zircuit_ethena_usdt",
  name: "Earn points on Zircuit using USDT",
  description: "Swap USDT to USDe on Uniswap and stake to Zircuit",
  website: {
    title: "Zircuit",
    url: "https://www.zircuit.com/",
  },
  tags: ["DeFi", "USDT", "USDe", "Points", "Uniswap"].map((name) => ({ title: name } as Tag)),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: 1,
  atomic: true,
  renderExpiry: undefined,
  inputs: [
    {
      name: "USDT Amount",
      inputType: InputType.ERC20Amount,
      description: "Amount to buy USDe",
      validate: decimalValidator(6, BigInt(0)),
      options: {
        token: "0xdAC17F958D2ee523a2206206994597C13D831ec7",
      },
    },
  ],
  render: async (context: Context) => {
    const txs: Tx[] = [];

    const usdtAddr = "0xdAC17F958D2ee523a2206206994597C13D831ec7";
    const usdeAddr = "0x4c9edd5852cd905f086c759e8383e09bff1e68b3";
    const uniswapQuoterAddr = "0xb27308f9F90D607463bb33eA1BeBb41C27CE5AB6";
    const uniswapRouterAddr = "0xE592427A0AEce92De3Edee1F18E0157C05861564";
    const zircuitRestakingPoolAddr = "0xF047ab4c75cebf0eB9ed34Ae2c186f3611aEAfa6";

    const client = createPublicClient({
      chain: context.chain,
      transport: http(),
    });

    const usdt = getContract({
      address: usdtAddr,
      abi: USDT,
      client: client,
    });

    const usde = getContract({
      address: usdeAddr,
      abi: USDe,
      client: client,
    });

    const uniswapQuoter = getContract({
      address: uniswapQuoterAddr,
      abi: UniswapQuoterV3,
      client: client,
    });

    const uniswapRouter = getContract({
      address: uniswapRouterAddr,
      abi: UniswapRouterV3,
      client: client,
    });

    const zircuitRestakingPool = getContract({
      address: zircuitRestakingPoolAddr,
      abi: ZircuitRestakingPool,
      client: client,
    });

    const usdtAllowance = await usdt.read.allowance([
      context.account.address,
      uniswapRouter.address,
    ]) as bigint;

    const usdtAmount = parseUnits(context.inputs[0], 6);

    if (usdtAmount > usdtAllowance) {
      txs.push({
        name: `Approve USDT`,
        description: `Approve ${context.inputs[0]} USDT for Uniswap`,
        to: usdt.address,
        value: 0n,
        data: encodeFunctionData({
          abi: usdt.abi,
          functionName: "approve",
          args: [uniswapRouter.address, usdtAmount],
        }),
        abi: usdt.abi as Abi,
      });
    }

    const usdeAmount = await uniswapQuoter.read.quoteExactInputSingle([
      usdtAddr, // token in
      usdeAddr, // token out
      "100", // fee
      usdtAmount, // amount in
      "0" // sqrtPriceLimitX96
    ]) as bigint;

    const usdeFormatAmount = formatUnits(usdeAmount, 18);

    txs.push({
      name: `Buy USDe`,
      description: `Swap ${context.inputs[0]} USDT to ${usdeFormatAmount} USDe`,
      to: uniswapRouter.address,
      value: 0n,
      data: encodeFunctionData({
        abi: uniswapRouter.abi,
        functionName: "exactInputSingle",
        args: [[
          usdtAddr, // token in
          usdeAddr, // token out
          "100", // fee
          context.account.address, // recipient
          Math.floor(Date.now() / 1000) + 300, // deadline
          usdtAmount, // amountIn
          usdeAmount, // amountOutMinimum
          "0" // sqrtPriceLimitX96
        ]],
      }),
      abi: uniswapRouter.abi as Abi,
    });

    const usdeAllowance = await usde.read.allowance([
      context.account.address,
      zircuitRestakingPoolAddr,
    ]) as bigint;

    if (usdeAmount > usdeAllowance) {
      txs.push({
        name: `Approve USDe`,
        description: `Approve ${usdeFormatAmount} USDe for Zircuit`,
        to: usde.address,
        value: 0n,
        data: encodeFunctionData({
          abi: usde.abi,
          functionName: "approve",
          args: [zircuitRestakingPoolAddr, usdeAmount],
        }),
        abi: usde.abi as Abi,
      });
    }

    txs.push({
      name: `Stake USDe`,
      description: `Stake ${usdeFormatAmount} USDe to Zircuit`,
      to: zircuitRestakingPool.address,
      value: 0n,
      data: encodeFunctionData({
        abi: zircuitRestakingPool.abi,
        functionName: "depositFor",
        args: [
          usdeAddr,
          context.account.address,
          usdeAmount,
        ],
      }),
      abi: zircuitRestakingPool.abi as Abi,
    });

    return txs;
  }
};

export default usdtCase;
