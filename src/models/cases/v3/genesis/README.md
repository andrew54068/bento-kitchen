# GenesisLRT

### Notes

The current `cToken` is `genETH`, but please note that it might change in the future.

### Steps

##### 1. Stake with GenesisLRT

Stake with GenesisLRT and receive `genETH` tokens.

> Minimum is 100 wei (0.0000000000000001 ETH)

- to: `0x46199caa0e453971cedf97f926368d9e5415831a`
- func: stake (`0x3a4b66f1`)
- params: code(`bytes32`)
- [Transaction](https://etherscan.io/tx/0x7a8d6c2c2f4e64764474c131ddf23207083ffa4134a4d277eb721a32b6283772)

##### 2. Stake with Lido

Stake with Lido and receive `stETH` tokens.

- func: submit (`0xa1903eab`)
- params: referrer(`address`)
- [Transaction](https://etherscan.io/tx/0x32fa5f609ee9e6b26b1dcc7da39cd76034db54b617e5e4ed054d2b46fd9ff281)

#### 3. Approve to wrap `stETH`

Approve received `stETH` to be wrapped to `wstETH`.

- func: approve (`0x095ea7b3`)
- params: spender(`address`), amount(`uint256`)
- [Transaction](https://etherscan.io/tx/0x0077b1e22c3cc8687191804e66913664e75100f276fbd733fa85638c7732b618)

#### 4. Wrap `stETH` to `wstETH`

Wrap `stETH` to `wstETH`.

- func: wrap (`0x0d9cbea6`)
- params: stETHAmount(`uint256`)
- [Transaction](https://etherscan.io/tx/0x08b3b3d35b2e9fc0bafcd83354419630e8d18491dcfeef57c8486b9f4e5f3af1)

#### 5. Approve to deposit `wstETH` to Uniswap Pool

Approve `wstETH` to be deposited to Uniswap Pool.

- func: approve (`0x095ea7b3`)
- params: spender(`address`), amount(`uint256`)
- [Transaction](https://etherscan.io/tx/0x77790e05633bf63fb304378c08e1bd3dbf1ce4427cfa77f1ba07b67f3cdd4cf2)

#### 6. Approve to deposit `genETH` to Uniswap Pool

Approve `genETH` to be deposited to Uniswap Pool.

- func: approve (`0x095ea7b3`)
- params: spender(`address`), amount(`uint256`)
- [Transaction](https://etherscan.io/tx/0x8f7d89d6224e4b877a8d1af79c683352582fe6f87385afc501f153202f570a07)

#### 7. Deposit `wstETH` and `genETH` to Uniswap Pool

Deposit `wstETH` and `genETH` to Uniswap Pool.

- func: mint (`0x88316456`)
- params: Refer to the [Data Example](#data-example)

##### Data Example:

```
0x88316456
0000000000000000000000007f39c581f595b53c5cb19bd0b3f8da6c935e2ca0 -> token0          (wstETH contract address)
000000000000000000000000f073bac22dab7faf4a3dd6c6189a70d54110525c -> token1          (genETH contract address)
00000000000000000000000000000000000000000000000000000000000001f4 -> fee             (fixed 500 wei)
00000000000000000000000000000000000000000000000000000000000005aa -> tickLower       (calculated from tick)
00000000000000000000000000000000000000000000000000000000000005be -> tickUpper       (calculated from tick)
000000000000000000000000000000000000000000000000000386acdb5af69e -> amount0Desired  (desired wstETH amount)
000000000000000000000000000000000000000000000000000389906dea433a -> amount1Desired  (desired genETH amount)
0000000000000000000000000000000000000000000000000000000000000000 -> amount0Min      (0)
0000000000000000000000000000000000000000000000000000000000000000 -> amount1Min      (0)
000000000000000000000000436f795b64e23e6ce7792af4923a68afd3967952 -> recipient       (account address)
00000000000000000000000000000000000000000000000000000000660e601b -> deadline        (10 mins from now)
```

### References

- [GenesisLRT](https://genesislrt.com/)
- [Lido](https://lido.fi/)
- [Uniswap wstETH/genETH Pool](https://info.uniswap.org/#/pools/0x3c0a1a9e0e22b9acc9248d9f358286e9e9205b0a)
- [wstETH Contract](https://etherscan.io/address/0x7f39C581F595B53c5cb19bD0b3f8dA6c935E2Ca0)
- [genETH Contract](https://etherscan.io/address/0xf073bAC22DAb7FaF4a3Dd6c6189a70D54110525C)
- [Uniswap wstETH/genETH Pool Contract](https://etherscan.io/address/0x3c0a1a9e0e22b9acc9248d9f358286e9e9205b0a)
- [Uniswap tick](https://blog.uniswap.org/uniswap-v3-math-primer#relationship-between-tick-and-sqrtprice)
