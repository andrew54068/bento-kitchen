import { Context, BatchCase, InputType, Tag, Tx } from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import { parseUnits, formatUnits } from "viem";

const pendlePoints: BatchCase = {
  id: "pendle_points",
  name: "Earn Points on Pendle",
  description: "Purchase YT rsETH and YT ezETH to earn Kelp Miles, Renzo ezPoints and EigenLayer Points",
  website: {
    title: "Pendle",
    url: "https://app.pendle.finance/points",
  },
  tags: ["DeFi", "ETH", "Points", "Kelp Dao", "Renzo Protocol"].map((name) => ({ title: name } as Tag)),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: 42161,
  atomic: true,
  renderExpiry: undefined,
  inputs: [
    {
      name: "ETH Amount",
      inputType: InputType.NativeAmount,
      description: "Amount to purchase YT rsETH",
      validate: decimalValidator(18, BigInt(0)),
    },
    {
      name: "ETH Amount",
      inputType: InputType.NativeAmount,
      description: "Amount to purchase YT ezETH",
      validate: decimalValidator(18, BigInt(0)),
    },
  ],
  render: async (context: Context) => {
    const txs: Tx[] = [];

    const rsMarketAddr = "0x6F02C88650837C8dfe89F66723c4743E9cF833cd";
    const ezMarketAddr = "0x60712e3C9136CF411C561b4E948d4d26637561e7";

    const ethTokenAddr = "0x0000000000000000000000000000000000000000";
    const rsEthTokenAddr = "0x4186BFC76E2E237523CBC30FD220FE055156b41F";
    const ezEthTokenAddr = "0x2416092f143378750bb29b79eD961ab195CcEea5";

    const ytTokenNames = ["YT rsETH", "YT ezETH"];
    const apiUrl = "https://api-v2.pendle.finance/sdk/api/v1/swapExactTokenForYt?";
    const slippage = "0.005";

    const queries = [
      // rsETH
      new URLSearchParams({
        chainId: context.chain.id.toString(),
        receiverAddr: context.account.address || "",
        marketAddr: rsMarketAddr,
        tokenInAddr: ethTokenAddr,
        amountTokenIn: parseUnits(context.inputs[0], 18).toString(),
        syTokenInAddr: rsEthTokenAddr,
        slippage: slippage
      }),
      // ezETH
      new URLSearchParams({
        chainId: context.chain.id.toString(),
        receiverAddr: context.account.address || "",
        marketAddr: ezMarketAddr,
        tokenInAddr: ethTokenAddr,
        amountTokenIn: parseUnits(context.inputs[1], 18).toString(),
        syTokenInAddr: ezEthTokenAddr,
        slippage: slippage
      })
    ];

    await Promise.all(
      queries.map(async (query, index) => {
        const {to, data, ytAmount} = await swapExactTokenForYt(apiUrl + query);
        txs.push({
          name: `Purchase ${ytTokenNames[index]}`,
          description: `Swap ${context.inputs[index]} ETH to ${formatUnits(ytAmount, 18)} ${ytTokenNames[index]}`,
          to: to,
          value: parseUnits(context.inputs[index], 18),
          data: data,
        });
      })
    );

    return txs;
  }
};

const swapExactTokenForYt = async (request: string | URL | Request) => {
  const response = await fetch(request);

  if (!response.ok) {
    const error = await response.json()
    throw new Error(`${response.status}: ${error.message}`);
  }

  const json = await response.json();
  return {
    to: json.transaction.to,
    data: json.transaction.data,
    ytAmount: json.data.amountYtOut
  };
}

export default [pendlePoints];
