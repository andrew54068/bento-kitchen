import type { Abi } from "abitype";
import {
  Context,
  BatchCase,
  InputType,
  Tag,
  Tx,
} from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import {
  createPublicClient,
  encodeAbiParameters,
  encodeFunctionData,
  getContract,
  http,
  parseUnits,
  PublicClient,
} from "viem";
import { CrocEnv, CrocSwapPlan } from "@crocswap-libs/sdk";
import ERC20 from "@/models/abi/ERC20.json";
import AmbientSwap from "./abi/AmbientSwap.json";
import WrappedEther from "./abi/WrappedEther.json";
import CogPair from "./abi/CogPair.json";
import Penpad from "./abi/Penpad.json";
import SyncswapClassicPoolFactory from "./abi/SyncSwapClassicPoolFactory.json";
import SyncswapClassicPool from "./abi/SyncSwapClassicPool.json";
import SyncswapRouter from "./abi/SyncSwapRouter.json";
import SpaceFi from "./abi/SpaceFi.json";
import { BigNumber } from "ethers";
import { fetchRouteSummary, postBuildRoute, RouteData } from "./kyberswap";

const scrollChainID = 0x82750;

const zeroAddress = "0x0000000000000000000000000000000000000000";
// NOTE: following address only for Scroll chain
const ethAddress = "0x0000000000000000000000000000000000000000";
const usdcAddress = "0x06eFdBFf2a14a7c8E15944D1F4A48F9F95F663A4";
const daiAddress = "0xcA77eB3fEFe3725Dc33bccB54eDEFc3D9f764f97";
const usdtAddress = "0xf55BEC9cafDbE8730f096Aa55dad6D22d44099Df";
const wETHAddress = "0x5300000000000000000000000000000000000004";
const ambientSwapAddress = "0xaaaaaaaacb71bf2c8cae522ea5fa455571a74106";
// cog finnace lend address
const cogPairUSDCWETH = "0x63fdafa50c09c49f594f47ea7194b721291ec50f";
const cogPairDAIWETH = "0x43187A6052A4BF10912CDe2c2f94953e39FcE8c7";
const cogPairUSDTWETH = "0x4Ac126e5dd1Cd496203a7E703495cAa8112A20cA";
// syncswap address
const syncswapClassicPoolFactoryAddress = "0x37BAc764494c8db4e54BDE72f6965beA9fa0AC2d";
const syncswapRouterAddress = "0x80e38291e06339d10AAB483C65695D004dBD5C69";
// kyber swap address
const kyberswapNativeEthAddress = "0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE";
// spacefi address
const spacefiRouterAddress = "0x18b71386418A9FCa5Ae7165E31c385a5130011b6";
// penpad staking address
const penpadStakingAddress = "0x8F53fA7928305Fd4f78c12BA9d9DE6B2420A2188";

const slippageTolerancePercentage = 1.5; // 1.5%
const slippageTolerance = slippageTolerancePercentage / 100;

const approveUSDCToAmbient: (amount: bigint) => Tx = (amount) => approveERC20({
  tokenAddress: usdcAddress,
  spenderAddress: ambientSwapAddress,
  amount: amount,
  name: "Approve USDC to Ambient Swap",
  description: "Approve USDC to Ambient Swap",
})

const approveWETHToCogPairUSDCWETH: (amount: bigint) => Tx = (amount) => approveERC20({
  tokenAddress: wETHAddress,
  spenderAddress: cogPairUSDCWETH,
  amount: amount,
  name: "Approve WETH to Cog Finance",
  description: "Approve WETH to Cog Finance (USDC/WETH)",
})

const approveUSDCToCogPairUSDCWETH: (amount: bigint) => Tx = (amount) => approveERC20({
  tokenAddress: usdcAddress,
  spenderAddress: cogPairUSDCWETH,
  amount: amount,
  name: "Approve USDC to Cog Finance",
  description: "Approve USDC to Cog Finance (USDC/WETH)",
})

const approveDAIToCogPairDAIWETH: (amount: bigint) => Tx = (amount) => approveERC20({
  tokenAddress: daiAddress,
  spenderAddress: cogPairDAIWETH,
  amount: amount,
  name: "Approve DAI to Cog Finance",
  description: "Approve DAI to Cog Finance (DAI/WETH)",
})

const approveUSDTToCogPairUSDTWETH: (amount: bigint) => Tx = (amount) => approveERC20({
  tokenAddress: usdtAddress,
  spenderAddress: cogPairUSDTWETH,
  amount: amount,
  name: "Approve USDT to Cog Finance",
  description: "Approve USDT to Cog Finance (USDT/WETH)",
})

const approveWETHToCogPairDAIWETH: (amount: bigint) => Tx = (amount) => approveERC20({
  tokenAddress: wETHAddress,
  spenderAddress: cogPairDAIWETH,
  amount: amount,
  name: "Approve WETH to Cog Finance",
  description: "Approve WETH to Cog Finance (DAI/WETH)",
})

const approveWETHToCogPairUSDTWETH: (amount: bigint) => Tx = (amount) => approveERC20({
  tokenAddress: wETHAddress,
  spenderAddress: cogPairUSDTWETH,
  amount: amount,
  name: "Approve WETH to Cog Finance",
  description: "Approve WETH to Cog Finance (USDT/WETH)",
})

const approveERC20: (params: {
  tokenAddress: `0x${string}`,
  spenderAddress: `0x${string}`,
  amount: bigint,
  name: string,
  description: string
}) => Tx = (params) => {
  const { tokenAddress, spenderAddress, amount, name, description } = params;
  return {
    name: name,
    description: description,
    to: tokenAddress,
    value: 0n,
    data: encodeFunctionData({
      abi: ERC20,
      functionName: 'approve',
      args: [
        spenderAddress,
        amount,
      ]
    }),
    abi: ERC20 as Abi,
  }
}

const wrapETH: (amount: bigint) => Tx = (amount) => ({
  name: "Wrap ETH",
  description: "Wrap ETH to WETH",
  to: wETHAddress,
  value: amount,
  data: encodeFunctionData({
    abi: WrappedEther,
    functionName: 'deposit',
    args: []
  }),
  abi: WrappedEther as Abi,
})

const unwrapETH: (amount: bigint) => Tx = (amount) => ({
  name: "Unwrap ETH",
  description: "Unwrap WETH to ETH",
  to: wETHAddress,
  value: 0n,
  data: encodeFunctionData({
    abi: WrappedEther,
    functionName: 'withdraw',
    args: [amount]
  }),
  abi: WrappedEther as Abi,
})

const swapUSDCToETHByAmbient: (amount: bigint) => Promise<{tx: Tx, plan: CrocSwapPlan}> = async (amount) => {
  const crocEnv = new CrocEnv(scrollChainID);
  const plan = crocEnv.sell(usdcAddress, BigNumber.from(amount.toString())).for(ethAddress, {
    slippage: slippageTolerance,
  });

  const cmd = encodeAbiParameters(
    [
      { type: "address" },
      { type: "address" },
      { type: "uint256" },
      { type: "bool", },
      { type: "bool" },
      { type: "uint128" },
      { type: "uint16", },
      { type: "uint128" },
      { type: "uint128" },
      { type: "uint8" },
    ],
    [
      plan.baseToken.tokenAddr as `0x${string}`,
      plan.quoteToken.tokenAddr as `0x${string}`,
      BigInt((await plan.context).chain.poolIndex),
      plan.sellBase,
      plan.qtyInBase,
      BigInt((await plan.qty).toString()),
      0,
      BigInt((await plan.calcLimitPrice()).toString()),
      BigInt((await plan.calcSlipQty()).toString()),
      0,
    ]
  );

  const tx: Tx = {
    name: "Swap USDC to ETH",
    description: "Swap USDC to ETH on Scroll using Ambient",
    to: ambientSwapAddress,
    value: 0n,
    data: encodeFunctionData({
      abi: AmbientSwap,
      functionName: 'userCmd',
      args: [
        1,
        cmd,
      ]
    }),
    abi: AmbientSwap as Abi,
  }

  return {
    tx,
    plan
  }
}

const swapETHToUSDCByAmbient: (amount: bigint) => Promise<{tx: Tx, plan: CrocSwapPlan}> = async (amount) => {
  const crocEnv = new CrocEnv(scrollChainID);
  const plan = crocEnv.sell(ethAddress, BigNumber.from(amount.toString())).for(usdcAddress, {
    slippage: slippageTolerance,
  });

  const usdcToETHCmd = encodeAbiParameters(
    [
      { type: "address" },
      { type: "address" },
      { type: "uint256" },
      { type: "bool", },
      { type: "bool" },
      { type: "uint128" },
      { type: "uint16", },
      { type: "uint128" },
      { type: "uint128" },
      { type: "uint8" },
    ],
    [
      plan.baseToken.tokenAddr as `0x${string}`,
      plan.quoteToken.tokenAddr as `0x${string}`,
      BigInt((await plan.context).chain.poolIndex),
      plan.sellBase,
      plan.qtyInBase,
      BigInt((await plan.qty).toString()),
      0,
      BigInt((await plan.calcLimitPrice()).toString()),
      BigInt((await plan.calcSlipQty()).toString()),
      0,
    ]
  );

  const tx: Tx = {
    name: "Swap ETH to USDC",
    description: "Swap ETH to USDC on Scroll using Ambient",
    to: ambientSwapAddress,
    value: amount,
    data: encodeFunctionData({
      abi: AmbientSwap,
      functionName: 'userCmd',
      args: [
        1,
        usdcToETHCmd,
      ]
    }),
    abi: AmbientSwap as Abi,
  }

  return {
    tx,
    plan
  }
}

const swapETHToUSDCBySyncswap: (client: PublicClient, userAddress: `0x${string}`, amount: bigint) => Promise<{tx: Tx, amountOutMin: bigint}> = async (client, userAddress, amount) => {
  const classicPoolFactory = getContract({
    address: syncswapClassicPoolFactoryAddress,
    abi: SyncswapClassicPoolFactory,
    client: client,
  });

  const poolAddress = await classicPoolFactory.read.getPool([wETHAddress, usdcAddress]) as `0x${string}`;

  if (poolAddress === zeroAddress) {
    throw new Error('Pool does not exist');
  }

  const pool = getContract({
    address: poolAddress,
    abi: SyncswapClassicPool,
    client: client,
  });
  let amountOutMin = await pool.read.getAmountOut([ethAddress, amount, userAddress]) as bigint;
  amountOutMin = amountOutMin * BigInt(980) / BigInt(1000); // 2% slippage

  // Constructs the swap paths with steps.
  // Determine withdraw mode, to withdraw native ETH or wETH on last step.
  // 0 - vault internal transfer
  // 1 - withdraw and unwrap to naitve ETH
  // 2 - withdraw and wrap to wETH
  const withdrawMode = 1; // 1 or 2 to withdraw to user's wallet

  const swapData = encodeAbiParameters(
    [
      { type: "address" },
      { type: "address" },
      { type: "uint8" },
    ],
    [
      wETHAddress,
      userAddress,
      withdrawMode,
    ]
  );

  const steps = [{
    pool: poolAddress,
    data: swapData,
    callback: zeroAddress,
    callbackData: '0x',
  }];

  const paths = [{
    steps: steps,
    tokenIn: ethAddress,
    amountIn: amount,
  }];
  
  const deadline = Math.floor(Date.now() / 1000) + 300; // 5 minutes from now

  const tx: Tx = {
    name: "Swap ETH to USDC",
    description: "Swap ETH to USDC on Scroll using Syncswap",
    to: syncswapRouterAddress,
    value: amount,
    data: encodeFunctionData({
      abi: SyncswapRouter,
      functionName: 'swap',
      args: [paths, amountOutMin, deadline]
      }),
      abi: SyncswapRouter as Abi,
  }

  return {
    tx,
    amountOutMin
  };
}


const approveUSDCToKyberSwap: (router: `0x${string}`, amount: bigint) => Tx = (router, amount) => approveERC20({
  tokenAddress: usdcAddress,
  spenderAddress: router,
  amount: amount,
  name: "Approve USDC to KyberSwap",
  description: "Approve USDC to KyberSwap Router",
})

const swapByRouteDataOnKyberSwap: (
  userAddress: `0x${string}`, routeData: RouteData,
) => Promise<Tx> = async (userAddress, routeData) => {

  let tx: Tx = {
    name: "Swap USDC to ETH",
    description: "Swap 50% USDC to ETH on Scroll using KyberSwap",
    to: routeData.routerAddress as `0x${string}`,
    value: 0n,
    data: routeData.data as `0x${string}`,
  }

  return tx;
}

const approveUSDCtoSpaceFi: (amount: bigint) => Tx = (amount) => approveERC20({
  tokenAddress: usdcAddress,
  spenderAddress: spacefiRouterAddress,
  amount: amount,
  name: "Approve USDC to SpaceFi",
  description: "Approve USDC to SpaceFi for swap",
})

const swapUSDCToETHBySpaceFi: (client: PublicClient, userAddress: `0x${string}`,  amountIn: bigint) => Promise<Tx> = async (client, userAddress, amountIn) => {
  const routerContract = getContract({
    address: spacefiRouterAddress,
    abi: SpaceFi,
    client: client,
  });

  let amountOuts = await routerContract.read.getAmountsOut([amountIn, [usdcAddress, wETHAddress]]) as bigint[];
  if (amountOuts.length < 1) {
    throw new Error('Failed to get amount out on SpaceFi');
  }
  let amountOutMin = amountOuts[0] * BigInt(9) / BigInt(10); // 10% slippage due to spacefi pool size is small

  let deadline = Math.floor(Date.now() / 1000) + 300; // 5 minutes from now

  let tx: Tx = {
    name: "Swap USDC to ETH",
    description: "Swap 50% USDC to ETH on Scroll using SpaceFi",
    to: spacefiRouterAddress,
    value: 0n,
    data: encodeFunctionData({
      abi: SpaceFi,
      functionName: 'swapExactTokensForETH',
      args: [amountIn, amountOutMin, [usdcAddress, wETHAddress], userAddress, deadline]
    }),
    abi: SpaceFi as Abi,
  }

  return tx;
}

const addWETHToCogUSDCWETHCollateral: (userAddress: `0x${string}`, amount: bigint) => Tx = (userAddress, amount) => addWETHToCogCollateral({
  userAddress,
  pairAddress: cogPairUSDCWETH,
  pair: "USDC/WETH",
  amount,
})

const addWETHToCogDAIWETHCollateral: (userAddress: `0x${string}`, amount: bigint) => Tx = (userAddress, amount) => addWETHToCogCollateral({
  userAddress,
  pairAddress: cogPairDAIWETH,
  pair: "DAI/WETH",
  amount,
})

const addWETHToCogUSDTWETHCollateral: (userAddress: `0x${string}`, amount: bigint) => Tx = (userAddress, amount) => addWETHToCogCollateral({
  userAddress,
  pairAddress: cogPairUSDTWETH,
  pair: "USDT/WETH",
  amount,
})

const addWETHToCogCollateral: (params: {
  userAddress: `0x${string}`,
  pairAddress: `0x${string}`,
  pair: string,
  amount: bigint,
}) => Tx = (params) => {
  const { userAddress, pairAddress, pair, amount } = params;
  return {
    name: "Add collateral to Cog Finance with WETH",
    description: `Add collateral to Cog Finance (${pair}) with WETH`,
    to: pairAddress,
    value: 0n,
    data: encodeFunctionData({
      abi: CogPair,
      functionName: 'add_collateral',
      args: [userAddress, amount]
    }),
    abi: CogPair as Abi,
  }
}

// const cogAddWethCollateral = (userAccount, wethAmount, contractAddr) => {
//   return {
//     name: "Add collateral to Cog Finance with WETH",
//     description: "Add collateral to Cog Finance with WETH",
//     to: contractAddr,
//     call: {
//       abi: {
//         name: "add_collateral",
//         type: "function",
//         inputs: [
//           {
//             name: "to",
//             type: "address",
//           },
//           {
//             name: "amount",
//             type: "uint256",
//           },
//         ],
//         outputs: [],
//         stateMutability: "nonpayable",
//       },
//       functionArgs: [
//         { name: "to", value: userAccount },
//         { name: "amount", value: wethAmount.toString() },
//       ],
//     },
//   };
// };

// const cogAddWethCollateralDaiWeth = async ({ userInputAmount, userAccount, setupData }) => {
//   const wethAmount =
//     setupData["cogAddWethCollateralDaiWeth"] === undefined ? userInputAmount : setupData["cogAddWethCollateralDaiWeth"];
//   return cogAddWethCollateral(userAccount, wethAmount, CogPairDaiWeth);
// };

const borrowUSDCFromCog: (client: PublicClient, userAddress: `0x${string}`, amount: bigint) => Promise<Tx> = (client, userAddress, amount) => borrowFromCog({
  client,
  userAddress,
  pairAddress: cogPairUSDCWETH,
  tokenAddress: usdcAddress,
  amount,
  symbol: "USDC"
})

const borrowDAIFromCog: (client: PublicClient, userAddress: `0x${string}`, amount: bigint) => Promise<Tx> = (client, userAddress, amount) => borrowFromCog({
  client,
  userAddress,
  pairAddress: cogPairDAIWETH,
  tokenAddress: daiAddress,
  amount,
  symbol: "DAI"
})

const borrowUSDTFromCog: (client: PublicClient, userAddress: `0x${string}`, amount: bigint) => Promise<Tx> = (client, userAddress, amount) => borrowFromCog({
  client,
  userAddress,
  pairAddress: cogPairUSDTWETH,
  tokenAddress: usdtAddress,
  amount,
  symbol: "USDT"
})

const borrowFromCog: (params: {
  client: PublicClient,
  userAddress: `0x${string}`,
  pairAddress: `0x${string}`,
  tokenAddress: `0x${string}`,
  amount: bigint,
  symbol: string,
}) => Promise<Tx> = async (params) => {
  const { client, userAddress, pairAddress, tokenAddress, amount, symbol } = params;

  const tokenContract = getContract({
    address: tokenAddress,
    abi: ERC20,
    client: client,
  });

  const tokenBalanceForPair = await tokenContract.read.balanceOf([pairAddress]) as bigint;

  if (tokenBalanceForPair < amount) {
    throw (`${symbol} NOT enough in Cog Finance Pair,only ${tokenBalanceForPair} left! Please wait or supply less amount!`);
  }

  return {
    name: `Borrow ${symbol} from Cog Finance`,
    description: `Borrow ${symbol} from Cog Finance in half of the collateral WETH`,
    to: pairAddress,
    value: 0n,
    data: encodeFunctionData({
      abi: CogPair,
      functionName: 'borrow',
      args: [amount, userAddress, userAddress]
    }),
    abi: CogPair as Abi,
  }
}

const repayUSDCToCog: (amount: bigint) => Tx = (amount) => repayToCog({
  pairAddress: cogPairUSDCWETH,
  amount: amount,
  symbol: "USDC",
  pair: "USDC/WETH"
})

const repayDAIToCog: (amount: bigint) => Tx = (amount) => repayToCog({
  pairAddress: cogPairDAIWETH,
  amount: amount,
  symbol: "DAI",
  pair: "DAI/WETH"
})

const repayUSDTToCog: (amount: bigint) => Tx = (amount) => repayToCog({
  pairAddress: cogPairUSDTWETH,
  amount: amount,
  symbol: "USDT",
  pair: "USDT/WETH"
})

const repayToCog: (params: {
  pairAddress: `0x${string}`,
  amount: bigint,
  symbol: string,
  pair: string,
}) => Tx = (params) => {
  const { pairAddress, amount, symbol, pair } = params;
  return {
    name: `Repay ${symbol} to Cog Finance`,
    description: `Repay ${symbol} to Cog Finance (${pair})`,
    to: pairAddress,
    value: 0n,
    data: encodeFunctionData({
      abi: CogPair,
      functionName: 'repay',
      args: [amount]
    }),
    abi: CogPair as Abi,
  }
};

const removeUSDCWETHCollateralFromCog: (amount: bigint, userAddress: `0x${string}`) => Tx = (amount, userAddress) => removeCollateralFromCog({
  pairAddress: cogPairUSDCWETH,
  userAddress,
  amount,
  stableTokenSymbol: "USDC",
})

const removeDAIWETHCollateralFromCog: (amount: bigint, userAddress: `0x${string}`) => Tx = (amount, userAddress) => removeCollateralFromCog({
  pairAddress: cogPairDAIWETH,
  userAddress,
  amount,
  stableTokenSymbol: "DAI",
})

const removeUSDTWETHCollateralFromCog: (amount: bigint, userAddress: `0x${string}`) => Tx = (amount, userAddress) => removeCollateralFromCog({
  pairAddress: cogPairUSDTWETH,
  userAddress,
  amount,
  stableTokenSymbol: "USDT",
})

const removeCollateralFromCog: (params: {
  pairAddress: `0x${string}`,
  userAddress: string,
  amount: bigint,
  stableTokenSymbol: string,
}) => Tx = (params) => {
  const { pairAddress, userAddress, amount, stableTokenSymbol } = params;
  return {
    name: "Remove WETH Collateral from Cog Finance",
    description: `Remove WETH Collateral from Cog Finance (${stableTokenSymbol}/WETH)`,
    to: pairAddress,
    value: 0n,
    data: encodeFunctionData({
      abi: CogPair,
      functionName: 'remove_collateral',
      args: [userAddress, amount]
    }),
    abi: CogPair as Abi,
  }
}

// refer from: https://scrollscan.com/address/0x63fdafa50c09c49f594f47ea7194b721291ec50f#code
const predictBorrowShareBase = async (client: PublicClient, pairAddress: `0x${string}`, amount: bigint, adjust: number = 1) => {
  const cogPairContract = getContract({
    address: pairAddress,
    abi: CogPair,
    client: client,
  });
  // total_brorrow check
  const { elastic: totalElastic, base: totalBase } = await cogPairContract.read.total_borrow() as { elastic: bigint, base: bigint };
  // BORROW_OPENING_FEE = 50
  // BORROW_OPENING_FEE_PRECISION = 100000
  // fee_amount = ( amount * BORROW_OPENING_FEE) / BORROW_OPENING_FEE_PRECISION
  const feeAmount = amount / BigInt(2000);

  // elastic = amount + fee_amount
  // base = (elastic * total_base) / total_elastic
  const elastic = amount + feeAmount;
  const base = elastic * totalBase / totalElastic;
  // for safe to repay, minus 3
  const accuracy = "1000000000000000000";
  const adjustDivide = BigInt(1.0 / adjust * Number(accuracy));
  return base * BigInt(accuracy) / adjustDivide - BigInt(3);
};

const depositToPenpad: (amount: bigint) => Tx = (amount) => {
  return {
    name: "Deposit to Penpad",
    description: "Deposit to Penpad staking contract",
    to: penpadStakingAddress,
    value: amount,
    data: encodeFunctionData({
      abi: Penpad,
      functionName: 'stake',
      args: []
    }),
    abi: Penpad as Abi,
  };
}

const scrollAirdropHuntingRookie: BatchCase = {
  id: "scroll_airdrop_hunting_rookie",
  name: "Airdrop Hunting (Rookie) on Scroll",
  description:
    "Airdrop Potential: ⭐⭐⭐; Number of Contract Interacted: 2; On-Chain Volume Boost: 4x; Click Saved: 4;",
  website: {
    title: "Scroll",
    url: "https://scroll.io/",
  },
  tags: ["Scroll", "Airdrop", "Swap"].map(
    (name) => ({ title: name } as Tag)
  ),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: scrollChainID,
  atomic: true,
  renderExpiry: 15,
  inputs: [
    {
      name: "USDC Amount",
      inputType: InputType.ERC20Amount,
      description:
        "Amount to swap, wrap and unwrap",
      validate: decimalValidator(6, BigInt(0)),
      options: {
        token: usdcAddress,
      },
    },
  ],
  render: async (context: Context) => {
    const inputUSDCAmount = parseUnits(context.inputs[0], 6);

    let txs: Tx[] = [];

    // 1
    txs.push(approveUSDCToAmbient(inputUSDCAmount))

    // 2
    const { tx: swapUSDCToETHTx, plan: ethToUSDCPlan } = await swapUSDCToETHByAmbient(inputUSDCAmount);
    txs.push(swapUSDCToETHTx)

    // 3
    const slipQty = parseFloat((await ethToUSDCPlan.impact).buyQty) * (1 - ethToUSDCPlan.slippage);
    const ethMinOut = BigInt((await ethToUSDCPlan.baseToken.roundQty(slipQty)).toString());
    txs.push(wrapETH(ethMinOut));

    // 4
    txs.push(unwrapETH(ethMinOut));

    // 5
    const { tx: swapETHToUSDCTx } = await swapETHToUSDCByAmbient(ethMinOut);
    txs.push(swapETHToUSDCTx);

    return txs;
  },
};

const scrollAirdropHuntingAdvanced: BatchCase = {
  id: "scroll_airdrop_hunting_advanced",
  name: "Airdrop Hunting (Advanced) on Scroll",
  description:
    "Airdrop Potential: ⭐⭐⭐⭐; Number of Contract Interacted: 7; On-Chain Volume Boost: 10x; Click Saved: 15;",
  website: {
    title: "Scroll",
    url: "https://scroll.io/",
  },
  tags: ["Scroll", "Airdrop", "Swap", "Lending"].map(
    (name) => ({ title: name } as Tag)
  ),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: scrollChainID,
  atomic: true,
  renderExpiry: 15,
  inputs: [
    {
      name: "USDC Amount",
      inputType: InputType.ERC20Amount,
      description:
        "Amount to swap, wrap, unwrap, lending and borrow",
      validate: decimalValidator(6, BigInt(0)),
      options: {
        token: usdcAddress,
      },
    },
  ],
  render: async (context: Context) => {
    const client: PublicClient = createPublicClient({
      chain: context.chain,
      transport: http(),
    });
    const inputUSDCAmount = parseUnits(context.inputs[0], 6);
    const userAddress = context.account.address as `0x${string}`

    let txs: Tx[] = [];

    // --- Swap to ETH ---
    // 1
    txs.push(approveUSDCToAmbient(inputUSDCAmount));

    // 2
    const { tx: swapUSDCToETHTx, plan: ethToUSDCPlan } = await swapUSDCToETHByAmbient(inputUSDCAmount);
    txs.push(swapUSDCToETHTx)

    // 3
    const slipQty = parseFloat((await ethToUSDCPlan.impact).buyQty) * (1 - ethToUSDCPlan.slippage);
    const ethMinOut = BigInt((await ethToUSDCPlan.baseToken.roundQty(slipQty)).toString());
    txs.push(wrapETH(ethMinOut));

    // --- Cog Finance (USDC/WETH) ---
    // 4
    txs.push(approveWETHToCogPairUSDCWETH(ethMinOut));

    // 5
    txs.push(addWETHToCogUSDCWETHCollateral(userAddress, ethMinOut));

    // 6
    const usdcBorrowAmount = inputUSDCAmount / BigInt(2)
    txs.push(await borrowUSDCFromCog(client, userAddress, usdcBorrowAmount));

    // 7
    txs.push(approveUSDCToCogPairUSDCWETH(usdcBorrowAmount * BigInt(1001) / BigInt(1000)));

    // 8
    const repayUSDCAmount = await predictBorrowShareBase(client, cogPairUSDCWETH, usdcBorrowAmount, 0.9992);
    txs.push(repayUSDCToCog(repayUSDCAmount))

    // 9
    const withdrawWETHAmountFromUSDCWETH = ethMinOut * BigInt(999) / BigInt(1000);
    txs.push(removeUSDCWETHCollateralFromCog(withdrawWETHAmountFromUSDCWETH, userAddress));

    // --- Cog Finance (DAI/WETH) ---
    // 10
    txs.push(approveWETHToCogPairDAIWETH(withdrawWETHAmountFromUSDCWETH));

    // 11
    txs.push(addWETHToCogDAIWETHCollateral(userAddress, withdrawWETHAmountFromUSDCWETH));

    // 12
    const daiBorrowAmount = usdcBorrowAmount * (BigInt(10) ** BigInt(12))
    txs.push(await borrowDAIFromCog(client, userAddress, daiBorrowAmount));

    // 13
    txs.push(approveDAIToCogPairDAIWETH(daiBorrowAmount));

    // 14
    const repayDAIAmount = await predictBorrowShareBase(client, cogPairDAIWETH, daiBorrowAmount, 0.9992);
    txs.push(repayDAIToCog(repayDAIAmount));

    // 15
    const withdrawWETHAmountFromDAIWETH = withdrawWETHAmountFromUSDCWETH * BigInt(9992) / BigInt(10000);
    txs.push(removeDAIWETHCollateralFromCog(withdrawWETHAmountFromDAIWETH, userAddress));

    // --- Swap Back to ETH ---
    // 16
    txs.push(unwrapETH(withdrawWETHAmountFromDAIWETH));

    // 17
    const { tx } = await swapETHToUSDCByAmbient(withdrawWETHAmountFromDAIWETH);
    txs.push(tx);

    return txs;
  },
};

const scrollAirdropHuntingProfessional: BatchCase = {
  id: "scroll_airdrop_hunting_professional",
  name: "Airdrop Hunting (Professional) on Scroll",
  description:
    "Airdrop Potential: ⭐⭐⭐⭐⭐; Number of Contract Interacted: 9; On-Chain Volume Boost: 13x; Click Saved: 21;",
  website: {
    title: "Scroll",
    url: "https://scroll.io/",
  },
  tags: ["Scroll", "Airdrop", "Swap", "Lending"].map(
    (name) => ({ title: name } as Tag)
  ),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: scrollChainID,
  atomic: true,
  renderExpiry: 15,
  inputs: [
    {
      name: "USDC Amount",
      inputType: InputType.ERC20Amount,
      description:
      "Amount to swap, wrap, unwrap, lending and borrow",
      validate: decimalValidator(6, BigInt(0)),
      options: {
        token: usdcAddress,
      },
    },
  ],
  render: async (context: Context) => {
    const client: PublicClient = createPublicClient({
      chain: context.chain,
      transport: http(),
    });
    const inputUSDCAmount = parseUnits(context.inputs[0], 6);
    const userAddress = context.account.address as `0x${string}`

    let txs: Tx[] = [];

    // --- Swap to ETH ---
    // 1
    txs.push(approveUSDCToAmbient(inputUSDCAmount));

    // 2
    const { tx: swapUSDCToETHTx, plan: ethToUSDCPlan } = await swapUSDCToETHByAmbient(inputUSDCAmount);
    txs.push(swapUSDCToETHTx)

    // 3
    const slipQty = parseFloat((await ethToUSDCPlan.impact).buyQty) * (1 - ethToUSDCPlan.slippage);
    const ethMinOut = BigInt((await ethToUSDCPlan.baseToken.roundQty(slipQty)).toString());
    txs.push(wrapETH(ethMinOut));

    // --- Cog Finance (USDC/WETH) ---
    // 4
    txs.push(approveWETHToCogPairUSDCWETH(ethMinOut));

    // 5
    txs.push(addWETHToCogUSDCWETHCollateral(userAddress, ethMinOut));

    // 6
    const usdcBorrowAmount = inputUSDCAmount / BigInt(2)
    txs.push(await borrowUSDCFromCog(client, userAddress, usdcBorrowAmount));

    // 7
    txs.push(approveUSDCToCogPairUSDCWETH(usdcBorrowAmount * BigInt(1001) / BigInt(1000)));

    // 8
    const repayUSDCAmount = await predictBorrowShareBase(client, cogPairUSDCWETH, usdcBorrowAmount, 0.9992);
    txs.push(repayUSDCToCog(repayUSDCAmount))

    // 9
    const withdrawWETHAmountFromUSDCWETH = ethMinOut * BigInt(999) / BigInt(1000);
    txs.push(removeUSDCWETHCollateralFromCog(withdrawWETHAmountFromUSDCWETH, userAddress));

    // --- Cog Finance (DAI/WETH) ---
    // 10
    txs.push(approveWETHToCogPairDAIWETH(withdrawWETHAmountFromUSDCWETH));

    // 11
    txs.push(addWETHToCogDAIWETHCollateral(userAddress, withdrawWETHAmountFromUSDCWETH));

    // 12
    const daiBorrowAmount = usdcBorrowAmount * (BigInt(10) ** BigInt(12))
    txs.push(await borrowDAIFromCog(client, userAddress, daiBorrowAmount));

    // 13
    txs.push(approveDAIToCogPairDAIWETH(daiBorrowAmount));

    // 14
    const repayDAIAmount = await predictBorrowShareBase(client, cogPairDAIWETH, daiBorrowAmount, 0.9992);
    txs.push(repayDAIToCog(repayDAIAmount));

    // 15
    const withdrawWETHAmountFromDAIWETH = withdrawWETHAmountFromUSDCWETH * BigInt(9992) / BigInt(10000);
    txs.push(removeDAIWETHCollateralFromCog(withdrawWETHAmountFromDAIWETH, userAddress));

    // --- Cog Finance (USDT/WETH) ---
    // 16
    txs.push(approveWETHToCogPairUSDTWETH(withdrawWETHAmountFromDAIWETH));

    // 17
    txs.push(addWETHToCogUSDTWETHCollateral(userAddress, withdrawWETHAmountFromDAIWETH));

    // 18
    const usdtBorrowAmount = usdcBorrowAmount;
    txs.push(await borrowUSDTFromCog(client, userAddress, usdtBorrowAmount));

    // 19
    txs.push(approveUSDTToCogPairUSDTWETH(usdtBorrowAmount));

    // 20
    const repayUSDTAmount = await predictBorrowShareBase(client, cogPairUSDTWETH, usdtBorrowAmount, 0.9988);
    txs.push(repayUSDTToCog(repayUSDTAmount));
    
    // 21
    const withdrawWETHAmountFromUSDTWETH = withdrawWETHAmountFromDAIWETH * BigInt(9988) / BigInt(10000);
    txs.push(removeUSDTWETHCollateralFromCog(withdrawWETHAmountFromUSDTWETH, userAddress));

    // --- Swap Back to ETH ---
    // 22
    txs.push(unwrapETH(withdrawWETHAmountFromUSDTWETH));

    // 23
    const { tx } = await swapETHToUSDCByAmbient(withdrawWETHAmountFromUSDTWETH);
    txs.push(tx);

    return txs;
  },
}

const scrollAirdropHuntingFromKOL: BatchCase = {
  id: "scroll_airdrop_hunting_with_penpad",
  name: "Airdrop Hunting on Scroll + Stake on PenPad",
  description:
    "Airdrop Potential: ⭐⭐⭐⭐⭐; Number of Contract Interacted: 4; Click Saved: 6;",
  website: {
    title: "Scroll",
    url: "https://scroll.io/",
  },
  tags: ["Scroll", "Airdrop", "Swap", "Launchpad"].map(
    (name) => ({ title: name } as Tag)
  ),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: scrollChainID,
  atomic: true,
  renderExpiry: 15,
  inputs: [
    {
      name: "ETH Amount",
      inputType: InputType.NativeAmount,
      description:
      "Amount to swap to USDC, then swap to ETH",
      validate: decimalValidator(18, BigInt(0), BigInt(0.1*10**18)),
    },{
      name: "ETH Amount",
      inputType: InputType.NativeAmount,
      description:
      "Amount to stake, require 0.05 eth minimum",
      validate: decimalValidator(18, BigInt(0.05*10**18)),
    },
  ],
  render: async (context: Context) => {
    const userAddress = context.account.address as `0x${string}`
    const client: PublicClient = createPublicClient({
      chain: context.chain,
      transport: http(),
    });
    
    // sanity check
    const balance = await client.getBalance({address: userAddress});
    const ethToSwap = parseUnits(context.inputs[0], 18);
    const ethToStake = parseUnits(context.inputs[1], 18);
    if (balance < ethToSwap + ethToStake) {
      throw new Error("Insufficient balance");
    }

    let txs: Tx[] = [];

    // 1
    const {tx, amountOutMin} = await swapETHToUSDCBySyncswap(client, userAddress, ethToSwap);
    txs.push(tx);

    // split half of the amountOutMin to swap
    const splitAmoutOut = BigInt(amountOutMin) / BigInt(2);
    const tokenOut = kyberswapNativeEthAddress;
    const tokenIn = usdcAddress;
    const routeSummary = await fetchRouteSummary(tokenIn, tokenOut, splitAmoutOut);
    const routeData = await postBuildRoute(routeSummary, userAddress, userAddress);

    // 2
    txs.push(approveUSDCToKyberSwap(routeData.routerAddress as `0x${string}`, BigInt(routeData.amountIn)));
    
    // 3
    txs.push(await swapByRouteDataOnKyberSwap(userAddress, routeData));

    // 4
    txs.push(approveUSDCtoSpaceFi(splitAmoutOut));

    // 5
    txs.push(await swapUSDCToETHBySpaceFi(client, context.account.address as `0x${string}`, splitAmoutOut));

    // 6
    txs.push(depositToPenpad(ethToStake));

    return txs;
  },
}

export default [
  scrollAirdropHuntingRookie,
  scrollAirdropHuntingAdvanced,
  scrollAirdropHuntingProfessional,
  scrollAirdropHuntingFromKOL,
];
