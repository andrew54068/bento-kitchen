import { Chain } from "wagmi/chains";
import type { Abi, AbiFunction, Address } from "abitype";
import { Hex } from "viem";
import { type UseAccountReturnType } from "wagmi";

export enum InputType {
  Text = "Text",
  ERC20Amount = "ERC20 Amount",
  NativeAmount = "Native Amount",
  Address = "Address",
}

export type ERC20AmountInputOptions = {
  token: Address;
};

export type Input = {
  name: string;
  description: string;
  inputType: InputType;
  validate?: (value: string) => void; // throw error if invalid
  options?: ERC20AmountInputOptions;
};

export type Context = {
  account: UseAccountReturnType;
  chain: Chain;
  inputs: string[];
};

export type TxRenderer = (context: Context) => Promise<Tx[]>;

export type Tx = {
  name: string;
  description: string;
  to: Address;
  value: BigInt;
  data?: Hex;
  abi?: Abi; // for decoding arguments from data
};

export type PreviewTx = {
  name: string;
  description: string;
  to: Address;
};

export type Tag = {
  title: string;
  url?: string;
};

export type BatchCase = {
  id: string;
  name: string;
  description: string;
  website: Tag;
  tags?: Tag[];
  curatorTwitter?: {
    name?: string;
    url: string;
  };
  networkId: number;
  atomic: boolean;
  renderExpiry?: number; // in seconds

  inputs?: Input[];
  render: TxRenderer;
  previewTx?: PreviewTx[];
};
