import type { Abi } from "abitype";
import {
  Context,
  BatchCase,
  InputType,
  Tag,
  Tx,
} from "@/models/cases/v3/types";
import { decimalValidator } from "@/models/cases/v3/utils";
import {
  getContract,
  formatEther,
  createPublicClient,
  encodeFunctionData,
  parseUnits,
  http,
} from "viem";
import swell from "./abi/swell.json";
import eigenpie from "./abi/eigenpie.json";
import mswETH from "./abi/mswETH.json";
import zircuit from "./abi/zircuit.json";

const eigenpieAndSwell: BatchCase = {
  id: "eigenpie_swell",
  name: "Restake with Eigenpie & Swell & Zircuit",
  description:
    "Earn EigenLayer Points + Daily GenesisLRT Gems + Liquidity x15 GenesisLRT Gems",
  website: {
    title: "Genesis",
    url: "https://www.genesislrt.com/",
  },
  tags: ["DeFi", "LRT", "ETH"].map((name) => ({ title: name } as Tag)),
  curatorTwitter: {
    name: "Bento Batch 🍱",
    url: "https://x.com/bentobatch",
  },
  networkId: 1,
  atomic: true,
  renderExpiry: 15,
  inputs: [
    {
      name: "ETH Amount",
      inputType: InputType.NativeAmount,
      description: "Amount to deposit",
      validate: decimalValidator(18, 0n),
    },
  ],
  render: async (context: Context) => {
    const txs: Tx[] = [];
    const ethInputAmount = parseUnits(context.inputs[0], 18);
    const eth = 10n ** 18n;

    // Addresses
    const swETHProxyAddr = "0xf951E335afb289353dc249e82926178EaC7DEd78"; // swell network: swETH
    const eigenpieRestakingProxyAddr =
      "0x24db6717dB1C75B9Db6eA47164D8730B63875dB7"; // eigenpie
    const mswETHProxyAddr = "0x32bd822d615A3658A68b6fDD30c2fcb2C996D678"; // mswETH
    const zircuitRestakingAddr = "0xF047ab4c75cebf0eB9ed34Ae2c186f3611aEAfa6"; // zircuit

    // Config
    const referer = "0x80011844928B469EAc5E4bC7e6EBA9b3C2Fa1b41"; // Bento Batch referer

    const client = createPublicClient({
      chain: context.chain,
      transport: http(),
    });

    const swellStaking = getContract({
      address: swETHProxyAddr,
      abi: swell,
      client: client,
    });

    const eigenpieRestaking = getContract({
      address: eigenpieRestakingProxyAddr,
      abi: eigenpie,
      client: client,
    });

    const mswETHToken = getContract({
      address: mswETHProxyAddr,
      abi: mswETH,
      client: client,
    });

    const zircuitRestaking = getContract({
      address: zircuitRestakingAddr,
      abi: zircuit,
      client: client,
    });

    const { result: ethToSwETHRate }: { result: bigint } =
      await swellStaking.simulate.ethToSwETHRate();
    const expectedSwETHAmount = (ethInputAmount * ethToSwETHRate) / eth;
    console.log(`expectedSwETHAmount: `, expectedSwETHAmount);

    // 1. Stake ETH with Swell with referer code
    txs.push({
      name: "Stake ETH with Swell",
      description: `Stake ${formatEther(
        ethInputAmount
      )} ETH and receive ${formatEther(expectedSwETHAmount)} swETH`,
      to: swellStaking.address,
      value: ethInputAmount,
      data: encodeFunctionData({
        abi: swellStaking.abi,
        functionName: "depositWithReferral",
        args: [referer],
      }),
      abi: swellStaking.abi as Abi,
    });

    // 2. Approve Swell to be restake to Eigenlayer on Eigenpie to earn Pearls
    const swETHDepositAmount = expectedSwETHAmount;
    const { result: swETHDepositAllowance } =
      await swellStaking.simulate.allowance([
        context.account.address,
        eigenpieRestaking.address,
      ]);
    if (swETHDepositAllowance < swETHDepositAmount) {
      txs.push({
        name: "Approve swETH to restake on Eigenpie",
        description: `Approve ${formatEther(
          swETHDepositAmount
        )} swETH to be restake to Eigenlayer on Eigenpie to earn Pearls`,
        to: swellStaking.address,
        value: 0n,
        data: encodeFunctionData({
          abi: swellStaking.abi,
          functionName: "approve",
          args: [
            eigenpieRestaking.address, // spender
            swETHDepositAmount, // amount
          ],
        }),
        abi: swellStaking.abi as Abi,
      });
    }

    // 3. Stake swETH with Eigenpie
    // swETH:mswETH should be 1:1
    const mswETHAmount = swETHDepositAmount;
    txs.push({
      name: "Restake swETH on Eigenpie",
      description: `Stake ${formatEther(
        swETHDepositAmount
      )} swETH and receive ${formatEther(mswETHAmount)} mswETH`,
      to: eigenpieRestaking.address,
      value: 0n,
      data: encodeFunctionData({
        abi: eigenpieRestaking.abi,
        functionName: "depositAsset",
        args: [
          swETHProxyAddr, // asset
          swETHDepositAmount, // depositAmount
          mswETHAmount, // minRec
          referer, // referral
        ],
      }),
      abi: eigenpieRestaking.abi as Abi,
    });

    const { result: isPreDeposit }: { result: boolean } =
      await eigenpieRestaking.simulate.isPreDeposit();
    console.log(`💥 isPreDeposit: ${JSON.stringify(isPreDeposit, null, '\t')}`)
    if (!isPreDeposit) {
      // 4. Approve mswETH to be restake to Zircuit to earn points
      const { result: mswETHDepositAllowance } =
        await mswETHToken.simulate.allowance([
          context.account.address,
          zircuitRestaking.address,
        ]);
      if (mswETHDepositAllowance < mswETHAmount) {
        txs.push({
          name: "Approve mswETH to restake on Zircuit",
          description: `Approve ${formatEther(
            mswETHAmount
          )} mswETH to be restaked on Zircuit to earn Zircuit points`,
          to: mswETHToken.address,
          value: 0n,
          data: encodeFunctionData({
            abi: mswETHToken.abi,
            functionName: "approve",
            args: [zircuitRestaking.address, mswETHAmount],
          }),
          abi: mswETHToken.abi as Abi,
        });
      }

      // 5. Restake mswETH with Zircuit
      txs.push({
        name: "Restake mswETH with Zircuit",
        description: `Restake ${formatEther(
          mswETHAmount
        )} mswETH to Zircuit to earn Zircuit points`,
        to: zircuitRestaking.address,
        value: 0n,
        data: encodeFunctionData({
          abi: zircuitRestaking.abi,
          functionName: "depositFor",
          args: [mswETHProxyAddr, context.account.address, mswETHAmount],
        }),
        abi: zircuitRestaking.abi as Abi,
      });
    }

    for (const tx of txs) {
      console.log(`tx: `, tx.name, tx.to, tx.value, tx.data);
    }

    return txs;
  },
};

export default [eigenpieAndSwell];
