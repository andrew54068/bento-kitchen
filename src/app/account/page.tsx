import { Box } from "@mantine/core";
import WalletInfo from "@/components/WalletInfo";

const Page = () => {
  return (
    <Box p="xl">
      <WalletInfo showChain />
    </Box>
  );
};
export default Page;
