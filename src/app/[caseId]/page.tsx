"use client";

import { usePathname } from "next/navigation";
import { useState, useCallback } from "react";
import CasePanel from "@/components/CasePanel/CasePanel";
import cases from "@/models/cases/v3";
import ErrorPage from "next/error";
import { Box } from "@mantine/core";

const playground = () => {
  const caseId = usePathname().split("/").pop();
  if (!caseId) {
    return <div />;
  }
  const batchCase = cases[caseId];
  if (!batchCase) {
    return <div />;
  }

  return (
    <Box h="100%" style={{ overflow: "auto" }}>
      <CasePanel key={caseId} batchCase={batchCase} />
    </Box>
  );
};

export default playground;
