import "@mantine/core/styles.css";
import "@mantine/notifications/styles.css";
import "@mantine/tiptap/styles.css";

import type { Metadata } from "next";
import Script from "next/script";
import { type ReactNode } from "react";
import { Providers } from "./providers";
import { MantineProvider } from "@mantine/core";
import { Notifications } from "@mantine/notifications";
import { light, geek } from "@/const/theme";
import App from "@/components/App";

export const metadata: Metadata = {
  title: "Bento Kitchen",
  description: "Alpha site for bento curators",
};

export default function RootLayout(props: { children: ReactNode }) {
  return (
    <html lang="en">
      <head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin=""
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Share+Tech+Mono&display=swap"
          rel="stylesheet"
        />
        <Script id="clarity">
          {`(function(c,l,a,r,i,t,y){
        c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
        t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
        y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
        })(window, document, "clarity", "script", "lcbsydss31");`}
        </Script>
      </head>
      <body>
        <Providers>
          <MantineProvider theme={geek}>
            <Notifications />
            <App>{props.children}</App>
          </MantineProvider>
        </Providers>
      </body>
    </html>
  );
}
