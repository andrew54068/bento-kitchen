import { http, createConfig } from "wagmi";
import {
  mainnet,
  polygon,
  arbitrum,
  optimism,
  base,
  scroll,
  avalanche,
  bsc,
  zora,
  blast,
} from "wagmi/chains";
import { coinbaseWallet, injected, walletConnect } from "wagmi/connectors";
import { blocto } from "@blocto/wagmi-connector";

export const config = createConfig({
  chains: [
    mainnet,
    polygon,
    arbitrum,
    optimism,
    base,
    scroll,
    avalanche,
    bsc,
    zora,
    blast,
  ],
  multiInjectedProviderDiscovery: false,
  connectors: [blocto({ appId: "___" })],
  ssr: true,
  transports: {
    [mainnet.id]: http(),
    [polygon.id]: http(),
    [arbitrum.id]: http(),
    [optimism.id]: http(),
    [base.id]: http(),
    [scroll.id]: http(),
    [avalanche.id]: http(),
    [bsc.id]: http(),
    [zora.id]: http(),
    [blast.id]: http(),
  },
});

declare module "wagmi" {
  interface Register {
    config: typeof config;
  }
}
