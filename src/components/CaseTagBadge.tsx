import { Badge } from "@mantine/core";
import crypto from "crypto";
import {
  Tag,
} from "@/models/cases/v3/types";

const mantineColorList = [
  "gray",
  "red",
  "pink",
  "grape",
  "violet",
  "indigo",
  "blue",
  "cyan",
  "teal",
  "green",
  "lime",
  "yellow",
  "orange",
];

const tagColor = (tag: string) => {
  const hash = crypto.createHash("md5").update(tag).digest().readUInt32LE(0);
  // use the number to select a color from the list
  const colorIndex = hash % mantineColorList.length;
  return mantineColorList[colorIndex];
};

const CaseTagBadge = ({ tag: tag }: { tag: Tag }) => {
  let badge;
  const url = tag.url;

  if (url === undefined || url === "") {
    badge = <Badge
      color="#EBEBEB"
      style={{
        color: "black",
        fontFamily: "sans-serif",
        fontWeight: "400",
        textTransform: "inherit",
        fontSize: "12px",
        padding: "4px 10px",
        lineHeight: "14px",
        height: "auto",
      }}
    >
      {tag.title}
    </Badge>;
  } else {
    badge =
      <Badge
        color="black"
        style={{
          fontFamily: "sans-serif",
          fontWeight: "400",
          textTransform: "inherit",
          fontSize: "12px",
          padding: "4px 10px",
          lineHeight: "14px",
          height: "auto",
          cursor: "pointer",
        }}
        onClick={() => url !== undefined && window.open(url, "_blank")}
      >
        {tag.title}
      </Badge>;
  }
  return (
    <div>
      {badge}
    </div>
  );
};

export default CaseTagBadge;
