import { Badge, Flex, Divider } from "@mantine/core";
import WebsiteIcon from "@/components/Icons/WebsiteIcon";

const WebsiteBadge = ({ title, url }: { title: string; url?: string }) => {
  return (
    <>
      <Badge
        color="black"
        style={{
          fontFamily: "sans-serif",
          fontWeight: "400",
          textTransform: "inherit",
          fontSize: "12px",
          padding: "4px 10px",
          lineHeight: "14px",
          height: "auto",
          cursor: "pointer",
        }}
        onClick={() => url !== undefined && window.open(url, "_blank")}
      >
        <Flex align="center" gap="5px">
          <WebsiteIcon size="14px" />
          {title}
        </Flex>
      </Badge>
      <Divider
        orientation="vertical"
        color="#AEAEAE"
        my="3px"
        display="absolute"
      />
    </>
  );
};

export default WebsiteBadge;
