import { useState, useEffect } from "react";
import { Chain } from "wagmi/chains";

import { Box, Text, TextInput, Button, Group, Loader } from "@mantine/core";
import { notifications } from "@mantine/notifications";

import {
  Input,
  InputType,
  ERC20AmountInputOptions,
} from "@/models/cases/v3/types";

import { useConfig, useAccount, useBalance } from "wagmi";

import {
  toHex,
  formatUnits,
  getContract,
  createPublicClient,
  http,
} from "viem";

import ERC20 from "@/models/abi/ERC20.json";

const TextInputField = ({
  input,
  value,
  error,
  setValue,
  setError,
}: {
  input: Input;
  value: string;
  error?: Error;
  setValue: (value: string) => void;
  setError: (error: Error | undefined) => void;
}) => {
  return (
    <TextInput
      label={input.name}
      description={input.description}
      // placeholder={input.placeholder}
      value={value}
      error={error?.message}
      onChange={(event) => {
        setValue(event.target.value);
        try {
          input.validate?.(event.target.value);
          setError(undefined);
        } catch (e) {
          setError(e as Error);
        }
      }}
    />
  );
};

const ERC20AmountInputField = ({
  chain,
  input,
  value,
  error,
  setValue,
  setError,
}: {
  chain: Chain;
  input: Input;
  value: string;
  error?: Error;
  setValue: (value: string) => void;
  setError: (error: Error | undefined) => void;
}) => {
  let options = input.options as ERC20AmountInputOptions | undefined;
  const client = createPublicClient({
    chain,
    transport: http(),
  });
  const account = useAccount();
  const [loading, setLoading] = useState(false); // loading state for fetching token details
  const [maxloading, setMaxLoading] = useState(false); // loading state for fetching max balance
  const [balanceStr, setBalanceStr] = useState("0");
  const [tokenName, setTokenName] = useState<string | undefined>(undefined);
  useEffect(() => {
    if (options?.token === undefined) {
      return;
    }
    const erc20 = getContract({
      address: options!.token,
      abi: ERC20,
      client,
    });
    const fetchTokenDetails = async () => {
      setLoading(true);
      const name = (await erc20.read.name()) as string;
      setTokenName(name);
    };
    fetchTokenDetails()
      .catch((e) => {
        notifications.show({
          id: "error",
          withCloseButton: true,
          autoClose: 2000,
          title: "Fetch Token Details Error",
          color: "red",
          message: (e as Error)?.message || "unknown error",
          loading: false,
        });
        setTokenName("Unknown Token");
      })
      .finally(() => setLoading(false));
  }, [chain, input.options]);
  useEffect(() => {
    if (account.status !== "connected") {
      return;
    }
    if (options?.token === undefined) {
      return;
    }
    const fetchBalance = async () => {
      const erc20 = getContract({
        address: options!.token,
        abi: ERC20,
        client,
      });
      const balance = (await erc20.read.balanceOf([account.address])) as bigint;
      const decimals = (await erc20.read.decimals()) as number;
      const fomattedBalance = formatUnits(balance, decimals);
      setBalanceStr(fomattedBalance);
    };
    setMaxLoading(true);
    fetchBalance()
      .catch((e) => {
        notifications.show({
          id: "error",
          withCloseButton: true,
          autoClose: 2000,
          title: "Fetch Balance Error",
          color: "red",
          message: (e as Error)?.message || "unknown error",
          loading: false,
        });
      })
      .finally(() => setMaxLoading(false));
  }, [account.address, account.status, input.options, chain]);
  return (
    <TextInput
      label={
        <Group>
          {tokenName === undefined ? "Token Amount" : `${tokenName} Amount`}
          {loading ? <Loader size="xs" /> : null}
        </Group>
      }
      description={input.description}
      placeholder={balanceStr}
      value={value}
      rightSection={
        <Box pr="md">
          <Button
            size="compact-xs"
            color="gray"
            loading={maxloading}
            disabled={maxloading}
            onClick={() => {
              setValue(balanceStr);
              setError(undefined);
            }}
          >
            Max
          </Button>
        </Box>
      }
      error={error?.message}
      onChange={(event) => {
        setValue(event.target.value);
        try {
          input.validate?.(event.target.value);
          setError(undefined);
        } catch (e) {
          setError(e as Error);
        }
      }}
    />
  );
};

const NativeAmountInputField = ({
  chain,
  input,
  value,
  error,
  setValue,
  setError,
}: {
  chain: Chain;
  input: Input;
  value: string;
  error?: Error;
  setValue: (value: string) => void;
  setError: (error: Error | undefined) => void;
}) => {
  const client = createPublicClient({
    chain,
    transport: http(),
  });
  const account = useAccount();
  // @ts-ignore
  const result = useBalance({ address: account.address, chainId: chain.id });
  const { data, isLoading } = result;
  return (
    <TextInput
      label={
        <Group>
          {data === undefined ? "Native Token Amount" : `${data.symbol} Amount`}
          {isLoading ? <Loader size="xs" /> : null}
        </Group>
      }
      description={input.description}
      placeholder={data?.formatted ?? "0"}
      value={value}
      rightSection={
        <Box pr="md">
          <Button
            size="compact-xs"
            color="gray"
            loading={isLoading}
            disabled={isLoading}
            onClick={() => {
              setValue(data?.formatted ?? "0");
              setError(undefined);
            }}
          >
            Max
          </Button>
        </Box>
      }
      error={error?.message}
      onChange={(event) => {
        setValue(event.target.value);
        try {
          input.validate?.(event.target.value);
          setError(undefined);
        } catch (e) {
          setError(e as Error);
        }
      }}
    />
  );
};

const AddressInputField = ({
  input,
  value,
  error,
  setValue,
  setError,
}: {
  input: Input;
  value: string;
  error?: Error;
  setValue: (value: string) => void;
  setError: (error: Error | undefined) => void;
}) => {
  const account = useAccount();
  return (
    <TextInput
      label={input.name}
      description={input.description}
      placeholder={account.address ?? "0x..."}
      value={value}
      rightSection={
        <Box pr="3.5em">
          <Button
            size="compact-xs"
            color="gray"
            disabled={account.status !== "connected"}
            onClick={() => {
              if (account.address !== undefined) {
                setValue(account.address);
              }
              setError(undefined);
            }}
          >
            My Address
          </Button>
        </Box>
      }
      error={error?.message}
      onChange={(event) => {
        setValue(event.target.value);
        try {
          input.validate?.(event.target.value);
          setError(undefined);
        } catch (e) {
          setError(e as Error);
        }
      }}
    />
  );
};

export {
  ERC20AmountInputField,
  NativeAmountInputField,
  TextInputField,
  AddressInputField,
};
