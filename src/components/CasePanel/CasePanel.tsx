import { useState, useEffect } from "react";
import {
  Box,
  Divider,
  Card,
  Flex,
  Title,
  Text,
  TextInput,
  Button,
  Space,
  Stack,
  Group,
  RingProgress,
} from "@mantine/core";
import { IconShare } from "@tabler/icons-react";
import { notifications } from "@mantine/notifications";
import {
  TxRenderer,
  Tx,
  BatchCase,
  Input,
  InputType,
  ERC20AmountInputOptions,
} from "@/models/cases/v3/types";
import BlockchainBadge from "@/components/BlockchainBadge";
import CaseTagBadge from "@/components/CaseTagBadge";
import WebsiteBadge from "@/components/WebsiteBadge";
import XIcon from "@/components/Icons/XIcon";
import BlueCheckIcon from "@/components/Icons/BlueCheckIcon";
import { useConfig, useAccount } from "wagmi";
import { switchNetwork, getWalletClient } from "@wagmi/core";
import { Chain } from "wagmi/chains";
import {
  toHex,
  formatUnits,
  getContract,
  createPublicClient,
  decodeFunctionData,
  http,
} from "viem";

import { useConnectModal } from "@/components/ConnectModal";
import WalletInfo from "@/components/WalletInfo";
import ERC20 from "@/models/abi/ERC20.json";
import { buildContext } from "@/models/cases/v3/utils";
import {
  TextInputField,
  AddressInputField,
  ERC20AmountInputField,
  NativeAmountInputField,
} from "./CaseInput";

type PanelProps = {
  batchCase: BatchCase;
  debug?: boolean;
};

const NamedInfoBox = ({ name, value }: { name: string; value: string }) => {
  return (
    <Stack gap="xs">
      <Text size="xs">{name}</Text>
      <Text style={{ wordBreak: "break-all" }}> {value}</Text>
    </Stack>
  );
};

type InputState = {
  value?: string;
  error?: Error;
};

const InputField = ({
  input,
  chain,
  value,
  error,
  setValue,
  setError,
}: {
  input: Input;
  chain: Chain;
  value: string;
  error?: Error;
  setValue: (value: string) => void;
  setError: (error: Error | undefined) => void;
}) => {
  switch (input.inputType) {
    case InputType.Text:
      return (
        <TextInputField
          input={input}
          value={value}
          error={error}
          setValue={setValue}
          setError={setError}
        />
      );
    case InputType.ERC20Amount:
      return (
        <ERC20AmountInputField
          chain={chain}
          input={input}
          value={value}
          error={error}
          setValue={setValue}
          setError={setError}
        />
      );
    case InputType.NativeAmount:
      return (
        <NativeAmountInputField
          chain={chain}
          input={input}
          value={value}
          error={error}
          setValue={setValue}
          setError={setError}
        />
      );
    case InputType.Address:
      return (
        <AddressInputField
          input={input}
          value={value}
          error={error}
          setValue={setValue}
          setError={setError}
        />
      );
    default:
      return null;
  }
};

const BatchTxInfo = ({ txs }: { txs: Tx[] }) => (
  <Stack>
    {txs.map((tx, i) => (
      <TxInfoCard key={i} tx={tx} i={i} />
    ))}
  </Stack>
);

const consoleColor = (log: "log" | "info" | "warn" | "error") => {
  switch (log) {
    case "log":
      return "white";
    case "info":
      return "green";
    case "warn":
      return "yellow";
    case "error":
      return "red";
    default:
      return "white";
  }
};

const TxInfoCard = ({ tx, i }: { tx: Tx; i: number }) => {
  const [compact, setCompact] = useState(true);
  return (
    <Card shadow="md" p="0" withBorder>
      <Stack
        p="md"
        gap="0"
        style={{ cursor: "pointer" }}
        onClick={() => setCompact(!compact)}
      >
        <Title order={5}>
          #{i + 1} {tx.name}
        </Title>
        <Text>{tx.description}</Text>
        {!compact ? <Divider mt="md" /> : null}
      </Stack>
      {!compact ? (
        <Stack p="0" gap="0">
          <Stack px="md" pb="md" gap="xs">
            <NamedInfoBox name="to" value={tx.to} />
            <NamedInfoBox name="value" value={tx.value.toString()} />
            {tx.data !== undefined ? (
              <NamedInfoBox name="data" value={tx.data!.toString()} />
            ) : null}
            {tx.data !== undefined && tx.abi !== undefined ? (
              <NamedInfoBox
                name="ABI decoded data"
                value={JSON.stringify(
                  decodeFunctionData({
                    abi: tx.abi,
                    data: tx.data,
                  }),
                  (key, value) => {
                    if (value instanceof Uint8Array) {
                      return `0x${Buffer.from(value).toString("hex")}`;
                    }
                    if (typeof value === "bigint") {
                      return value.toString();
                    }
                    return value;
                  }
                )}
              />
            ) : null}
          </Stack>
        </Stack>
      ) : null}
    </Card>
  );
};

const DetailView = ({
  batchCase,
  chain,
  inputValues,
  inputErrors,
  setInputErrors,
}: {
  batchCase: BatchCase;
  chain: Chain;
  inputValues: string[];
  inputErrors: (Error | undefined)[];
  setInputErrors: (next: (Error | undefined)[]) => void;
}) => {
  const [renderedTxs, setRenderedTxs] = useState<Tx[]>([]);
  const [genLoading, setGenLoading] = useState(false);
  const [sendLoading, setSendLoading] = useState(false);
  // rendered tx expired state
  const supportExpiry = batchCase.renderExpiry !== undefined;
  const [renderTime, setRenderTime] = useState(0); // unix timestamp
  const [isExpired, setIsExpired] = useState(false);
  const [countdownPercent, setCountdownPercent] = useState(100);
  if (supportExpiry) {
    // disable expiry for now
    useEffect(() => {
      const interval = 1000; // ms
      const timer = setInterval(() => {
        if (!supportExpiry) {
          return;
        }
        if (isExpired) {
          return;
        }
        const now = Math.floor(Date.now() / 1000);
        const elapsed = now - renderTime;
        if (elapsed >= batchCase.renderExpiry!) {
          setIsExpired(true);
          setCountdownPercent(0);
          return;
        }
        const percent = Math.floor(
          ((batchCase.renderExpiry! - elapsed) / batchCase.renderExpiry!) * 100
        );
        setCountdownPercent(percent);
      }, interval);
      return () => clearInterval(timer);
    });
  }
  // wagmi context
  const account = useAccount();
  // reset rendered txs account disconnect
  useEffect(() => {
    setRenderedTxs([]);
  }, [account.status]);
  const config = useConfig();
  return (
    <Stack>
      <Button
        color="dark.4"
        disabled={
          genLoading ||
          account.status !== "connected" ||
          account.chain?.id !== chain.id
        }
        loading={genLoading}
        variant="filled"
        onClick={async () => {
          setGenLoading(true);
          setRenderedTxs([]);
          try {
            // validate inputs
            let nextInputErrors = inputErrors.map((e) => e);
            for (let i = 0; i < (batchCase.inputs?.length ?? 0); i++) {
              const input = batchCase.inputs![i];
              try {
                input.validate?.(inputValues[i]);
                nextInputErrors[i] = undefined;
              } catch (e) {
                nextInputErrors[i] = e as Error;
              }
            }

            // check if any input has error
            setInputErrors(nextInputErrors);
            if (nextInputErrors.some((err) => err !== undefined)) {
              throw new Error("invalid inputs");
            }
            const _values = inputValues.map((v) => v);
            const context = buildContext(
              batchCase,
              account,
              _values,
              config.chains
            );
            const txs: Tx[] = await batchCase.render(context);
            setRenderedTxs(txs);
            setRenderTime(Math.floor(Date.now() / 1000));
            setCountdownPercent(100);
            setIsExpired(false);
          } catch (e) {
            notifications.show({
              id: "error",
              withCloseButton: true,
              autoClose: 2000,
              title: "Gen Batch Error",
              color: "red",
              message: (e as Error)?.message || "unknown error",
              loading: false,
            });
          }
          setGenLoading(false);
        }}
      >
        Gen Batch
        {account.status !== "connected"
          ? " (Connect Wallet Required)"
          : account.chain?.id !== chain.id
            ? ` (Switch to ${chain.name})`
            : ""}
      </Button>
      {account.status !== "connected" || renderedTxs.length <= 0 ? null : (
        <Box>
          <Divider />
          <Stack py="lg" gap="lg">
            <Flex
              direction="row"
              gap="md"
              justify="flex-start"
              align="flex-start"
              wrap="wrap"
            >
              <Stack style={{ flexGrow: 1 }} gap="xs">
                <Title order={4}>
                  Gen'd Batch ({renderedTxs.length} Tx
                  {renderedTxs.length > 1 ? "s" : ""})
                </Title>
                <Stack gap={0}>
                  <Text>Atomic: {batchCase.atomic ? "Yes" : "No"} </Text>
                  {supportExpiry ? (
                    <Text>Batch Expiry: {batchCase.renderExpiry} seconds </Text>
                  ) : null}
                </Stack>
              </Stack>
              {supportExpiry ? (
                <RingProgress
                  size={40}
                  thickness={4}
                  roundCaps
                  sections={[{ value: countdownPercent, color: "blue" }]}
                />
              ) : null}
            </Flex>
            <BatchTxInfo txs={renderedTxs} />
            <Button
              variant="gradient"
              gradient={{ from: "indigo", to: "red", deg: 10 }}
              loading={sendLoading}
              disabled={sendLoading || isExpired}
              onClick={async () => {
                setSendLoading(true);
                try {
                  const client = await getWalletClient(config);
                  let txRequests = await Promise.all(
                    renderedTxs.map(async (tx) => {
                      return client.prepareTransactionRequest({
                        to: tx.to,
                        // @ts-ignore
                        value: toHex(tx.value),
                        data: tx.data,
                        parameters: [],
                      });
                    })
                  );
                  let txHash = await client.request({
                    // @ts-ignore
                    method: "wallet_sendMultiCallTransaction",
                    // @ts-ignore
                    params: [txRequests, batchCase.atomic],
                  });
                  notifications.show({
                    id: "success",
                    withCloseButton: true,
                    autoClose: false,
                    color: "green",
                    title: "Send Batch Success",
                    message: "txHash: " + txHash,
                    loading: false,
                  });
                } catch (e) {
                  notifications.show({
                    id: "error",
                    withCloseButton: true,
                    autoClose: 2000,
                    title: "Send Batch Error",
                    color: "red",
                    message: (e as Error)?.message || "unknown error",
                    loading: false,
                  });
                }
                setSendLoading(false);
              }}
            >
              {isExpired ? " (Batch Expired. Please Gen Again)" : "Batcha!"}
            </Button>
          </Stack>
        </Box>
      )}
    </Stack>
  );
};

const Panel = ({ batchCase, debug }: PanelProps) => {
  const [inputValues, setInputValues] = useState<string[]>(
    new Array<string>(batchCase.inputs?.length ?? 0)
  );
  const [inputErrors, setInputErrors] = useState<(Error | undefined)[]>(
    new Array<Error | undefined>(batchCase.inputs?.length ?? 0)
  );
  const [sendLoading, setSendLoading] = useState(false);
  // wagmi context
  const account = useAccount();
  const config = useConfig();

  // share url
  const shareURL = window.location.href;

  // find the chain
  const chain = config.chains.find((c) => c.id === batchCase.networkId);
  if (chain === undefined) {
    throw new Error("chain not found");
  }

  return (
    <Box m="lg">
      <Stack gap="sm" pb="lg">
        {!!batchCase?.curatorTwitter?.url && (
          <a
            href={batchCase.curatorTwitter.url}
            target="_blank"
            rel="noreferrer"
            style={{
              textDecoration: "none",
              color: "black",
              fontFamily: "sans-serif",
            }}
          >
            <Flex gap="10px" align="center">
              <XIcon />
              <Text
                style={{
                  fontWeight: "400",
                  fontSize: "16px",
                }}
              >
                {batchCase?.curatorTwitter?.name}
              </Text>
              <BlueCheckIcon />
            </Flex>
          </a>
        )}
        <Title order={3}>{batchCase.name}</Title>
        <Flex
          gap="4px"
          style={{
            flexWrap: "wrap",
          }}
        >
          <WebsiteBadge
            title={batchCase.website?.title}
            url={batchCase.website.url}
          />
          <BlockchainBadge blockchain={chain} />
          {batchCase.tags?.map((tag) => (
            <CaseTagBadge tag={tag} key={tag.title} />
          ))}
        </Flex>
      </Stack>
      <Divider />
      <Box py="lg">
        <Stack gap="md">
          <div
            dangerouslySetInnerHTML={{
              __html: batchCase.description,
            }}
          />

          <Button
            variant="default"
            rightSection={<IconShare size={14} />}
            onClick={() => {
              if (navigator.share) {
                navigator.share({
                  title: "Share the batch case",
                  url: shareURL,
                });
                return;
              } else {
                navigator.clipboard.writeText(shareURL);
                document.execCommand("copy", true, shareURL);
                notifications.show({
                  id: "success",
                  withCloseButton: true,
                  autoClose: 2000,
                  color: "green",
                  title: "Copy URL",
                  message: shareURL,
                  loading: false,
                });
              }
            }}
          >
            Share
          </Button>
        </Stack>
      </Box>
      <Divider />
      <Stack py="lg">
        <Title order={4}>Wallet</Title>
        <WalletInfo showChain targetChain={chain} />
      </Stack>
      <Divider />
      <Stack py="lg" gap="lg">
        <Title order={4}>Parameters</Title>
        <Stack gap="sm">
          {batchCase.inputs?.map((input, i) => (
            <InputField
              key={i}
              input={input}
              chain={chain}
              value={inputValues[i]}
              error={inputErrors[i]}
              setValue={(value) => {
                setInputValues((prev) => {
                  const next = [...prev];
                  next[i] = value;
                  return next;
                });
              }}
              setError={(error) => {
                setInputErrors((prev) => {
                  const next = [...prev];
                  next[i] = error;
                  return next;
                });
              }}
            />
          )) ?? null}
        </Stack>
      </Stack>
      <DetailView
        batchCase={batchCase}
        chain={chain}
        inputValues={inputValues}
        inputErrors={inputErrors}
        setInputErrors={setInputErrors}
      />
    </Box>
  );
};

export default Panel;
