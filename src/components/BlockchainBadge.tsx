import { Badge } from "@mantine/core";
import {
  Chain,
  mainnet,
  polygon,
  arbitrum,
  optimism,
  base,
  scroll,
  avalanche,
  bsc,
  zora,
} from "wagmi/chains";

const blockchainColor = (blockchain: Chain) => {
  switch (blockchain) {
    case mainnet:
      return "gray";
    case polygon:
      return "grape";
    case arbitrum:
      return "blue";
    case optimism:
      return "red";
    case base:
      return "indigo";
    case scroll:
      return "yellow";
    case avalanche:
      return "red";
    case bsc:
      return "yellow";
    case zora:
      return "violet";
    default:
      return "gray";
  }
};

const BlockchainBadge = ({ blockchain }: { blockchain: Chain }) => {
  return (
    <Badge
      color="#EBEBEB"
      style={{
        color: "black",
        fontFamily: "sans-serif",
        fontWeight: "400",
        textTransform: "inherit",
        fontSize: "12px",
        padding: "4px 10px",
        lineHeight: "14px",
        height: "auto",
      }}
    >
      {blockchain.name}
    </Badge>
  );
};

export default BlockchainBadge;
