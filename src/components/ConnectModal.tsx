"use client";

import { createContext, useContext, useState, PropsWithChildren } from "react";
import { Connector, useConnect } from "wagmi";
import { Stack, Button, Modal } from "@mantine/core";

export const WalletOptions = () => {
  const { connectors, connect } = useConnect();
  const { close } = useConnectModal();
  return (
    <Stack>
      {connectors.map((connector) => (
        <Button
          color="dark.4"
          key={connector.uid}
          onClick={async () => {
            close();
            await connect({ connector });
          }}
        >
          {connector.name}
        </Button>
      ))}
    </Stack>
  );
};

export type ConnectModalContext = {
  opened: boolean;
  open: () => void;
  close: () => void;
};

const initialContext: ConnectModalContext = {
  opened: false,
  open: () => {},
  close: () => {},
};

const ModalContext = createContext(initialContext);

export const useConnectModal = () => useContext(ModalContext);

export const ConnectModalProvider = ({ children }: PropsWithChildren) => {
  const [opened, setOpened] = useState(false);
  const open = () => setOpened(true);
  const close = () => setOpened(false);
  return (
    <ModalContext.Provider value={{ opened, open, close }}>
      {children}
      <Modal opened={opened} onClose={close} zIndex={201}>
        <WalletOptions />
      </Modal>
    </ModalContext.Provider>
  );
};
