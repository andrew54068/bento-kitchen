interface IconProps extends React.ComponentPropsWithoutRef<"svg"> {
  size?: number | string;
}

export function Icon({ size = "16px", style, ...others }: IconProps) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 17 16"
      style={{ width: size, height: size, ...style }}
      {...others}
    >
      <rect x="0.0957031" width="16" height="16" rx="8" fill="#4EAAFF" />
      <path
        d="M12.0957 5L6.5957 10.5L4.0957 8"
        stroke="white"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default Icon;
