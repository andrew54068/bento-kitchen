interface IconProps extends React.ComponentPropsWithoutRef<"svg"> {
  size?: number | string;
}

export function Icon({ size = "25px", style, ...others }: IconProps) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 26 24"
      style={{ width: size, height: size, ...style }}
      {...others}
    >
      <rect width="25.0957" height="24" rx="4" fill="black" />
      <g clipPath="url(#clip0_98_6538)">
        <path
          d="M4.04174 4L10.6418 12.8268L4 20.0004H5.49482L11.3099 13.7188L16.0097 20.0007H21.0953L14.1235 10.679L20.3058 4H18.811L13.4557 9.78426L9.12839 4H4.04174ZM6.24012 5.1013H8.57704L18.8959 18.8991H16.5597L6.24012 5.1013Z"
          fill="white"
        />
      </g>
      <defs>
        <clipPath id="clip0_98_6538">
          <rect
            width="17.0957"
            height="16"
            fill="white"
            transform="translate(4 4)"
          />
        </clipPath>
      </defs>
    </svg>
  );
}

export default Icon;
