"use client";

import { useEffect } from "react";
import {
  useConfig,
  useAccount,
  useBalance,
  useConnect,
  useDisconnect,
} from "wagmi";
import { Chain } from "wagmi/chains";
import { switchChain } from "@wagmi/core";
import { useConnectModal } from "@/components/ConnectModal";
import {
  Box,
  Card,
  Group,
  Stack,
  Title,
  Text,
  Button,
  Flex,
} from "@mantine/core";
import { notifications } from "@mantine/notifications";

const WalletInfo = ({
  showChain,
  targetChain,
  onDisconnect,
}: {
  showChain: boolean;
  targetChain?: Chain;
  onDisconnect?: () => void;
}) => {
  const { connectors, connect, status, error } = useConnect();
  const { disconnect } = useDisconnect();
  const { open } = useConnectModal();
  const account = useAccount();
  const { data } = useBalance({
    address: account.address,
    // @ts-ignore
    chainId: account.chain?.id,
  });

  const config = useConfig();

  return (
    <Box w="100%">
      <Flex
        direction="row"
        gap="md"
        justify="flex-start"
        align="flex-end"
        wrap="wrap"
      >
        <Stack style={{ flexGrow: 1 }} gap="sm">
          {showChain ? (
            <Box>
              <Text size="xs">blockchain</Text>
              <Text>{account.chain?.name ?? "unknown"}</Text>
            </Box>
          ) : null}
          <Box>
            <Text size="xs">address</Text>
            <Text style={{ wordBreak: "break-all" }}>
              {account.address ?? "unknown"}
            </Text>
          </Box>
          <Box>
            <Text size="xs">balance</Text>
            <Text style={{ wordBreak: "break-all" }}>
              {data?.formatted === undefined
                ? "unknown"
                : `${data.formatted} ${data.symbol}`}
            </Text>
          </Box>
        </Stack>
        <Group>
          {account.status === "connected" &&
          targetChain !== undefined &&
          account.chain?.id !== targetChain.id ? (
            <Button
              color="dark.4"
              variant="filled"
              onClick={() =>
                // FIXME: check if target chain is supported by the wagmi config
                // @ts-ignore
                switchChain(config, { chainId: targetChain!.id }).catch((e) => {
                  notifications.show({
                    id: "error",
                    withCloseButton: true,
                    autoClose: 2000,
                    title: "Switch Chain Error",
                    color: "red",
                    message: (e as Error)?.message || "unknown error",
                    loading: false,
                  });
                })
              }
            >
              Swtich to {targetChain.name}
            </Button>
          ) : null}
          {account.status === "connected" ? (
            <Button
              variant="outline"
              color="red"
              onClick={async () => {
                await disconnect();
                if (onDisconnect) onDisconnect();
              }}
            >
              Disconnect
            </Button>
          ) : (
            <Button
              color="dark.4"
              onClick={() => {
                open();
              }}
            >
              Connect
            </Button>
          )}
        </Group>
      </Flex>
    </Box>
  );
};

export default WalletInfo;
