"use client";
import { type ReactNode } from "react";
import { usePathname } from "next/navigation";
import Link from "next/link";
import {
  AppShell,
  Burger,
  Box,
  Stack,
  Divider,
  NavLink,
  Group,
  Title,
  Text,
  ScrollArea,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { ConnectModalProvider } from "@/components/ConnectModal";
import { IconCompass, IconCode, IconWallet } from "@tabler/icons-react";
import cases from "@/models/cases/v3";

const App = ({ children }: { children: ReactNode }) => {
  const [opened, { toggle }] = useDisclosure();
  const pathname = usePathname();
  return (
    <AppShell
      layout="default"
      header={{ height: 60 }}
      navbar={{
        width: 300,
        collapsed: { mobile: !opened },
        breakpoint: "sm",
      }}
      footer={{ height: "1.5em" }}
    >
      <AppShell.Header>
        <Group h="100%" px="md">
          <Burger opened={opened} onClick={toggle} hiddenFrom="sm" size="sm" />
          <Title ta="center" order={3}>
            Bento Kitchen 👨‍🍳
          </Title>
        </Group>
      </AppShell.Header>
      <AppShell.Navbar>
        <ScrollArea>
          <Stack justify="space-between" h="100%" py="md">
            <Stack gap={0}>
              {Object.keys(cases).map((k) => (
                <NavLink
                  key={k}
                  px="md"
                  active={pathname === `/${k}`}
                  component={Link}
                  href={`/${k}`}
                  label={k}
                />
              ))}
            </Stack>
            <NavLink
              px="md"
              active={pathname === "/account"}
              component={Link}
              href="/account"
              label="Account"
              leftSection={<IconWallet size={20} stroke={1} />}
            />
          </Stack>
        </ScrollArea>
      </AppShell.Navbar>
      <AppShell.Main
        h="calc(100vh - var(--app-shell-header-height, 0px) - var(--app-shell-footer-height, 0px))"
        style={{ overflow: "hidden" }}
      >
        <ConnectModalProvider>{children}</ConnectModalProvider>
      </AppShell.Main>
      <AppShell.Footer>
        <Group h="100%" px="md" justify="space-between">
          <Text size="xs">© 2024 Bento</Text>
          <Text size="xs">made with ❤️ by seafood</Text>
        </Group>
      </AppShell.Footer>
    </AppShell>
  );
};

export default App;
